vdp package
===========

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   vdp.algorithms
   vdp.calibration
   vdp.components
   vdp.dynamics
   vdp.energy
   vdp.gps
   vdp.identification
   vdp.obdcan
   vdp.physics
   vdp.time
   vdp.trip
   vdp.utils

Module contents
---------------

.. automodule:: vdp
   :members:
   :undoc-members:
   :show-inheritance:
