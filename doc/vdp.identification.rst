vdp.identification package
==========================

Module contents
---------------

.. automodule:: vdp.identification
   :members:
   :undoc-members:
   :show-inheritance:
