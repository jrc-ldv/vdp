vdp.algorithms package
======================

Module contents
---------------

.. automodule:: vdp.algorithms
   :members:
   :undoc-members:
   :show-inheritance:
