.. figure:: ../doc/_static/images/logo.png
   :align: center
   :alt: alternate text
   :figclass: align-center

.. include:: ../README.rst
   :start-after: .. _start-info:
   :end-before: .. _end-info:

.. include:: ../README.rst
   :start-after: .. _start-sub:
   :end-before: .. _end-sub:

.. include:: ../AUTHORS.rst

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
