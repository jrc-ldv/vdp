Authors
=======

Developer:
    - Alessandro Tansini <Alessandro.TANSINI@ec.europa.eu>
    - Andres Marin <Andres.LAVERDE-MARIN@ext.ec.europa.eu>
Documentation:
    - Alessandro Tansini <Alessandro.TANSINI@ec.europa.eu>
    - Andrés Marín <Andres.LAVERDE-MARIN@ext.ec.europa.eu>
Model:
    - Alessandro Tansini <Alessandro.TANSINI@ec.europa.eu>
Supervision:
    - Georgios Fontaras <georgios.fontaras@ec.europa.eu>
    - Athina Mitsiara <Athina.MITSIARA@ext.ec.europa.eu>
Tester:
    - Dimitris Komnos <Dimitrios.KOMNOS@ec.europa.eu>