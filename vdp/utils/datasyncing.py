import numpy as np
import pandas as pd
from syncing.model import dsp as dsp_sync
from scipy.interpolate import interp1d


def align_and_merge_data(
    df_l,
    time_l_label,
    values_l_label,
    df_r,
    time_r_label,
    values_r_label,
    interpolation_method="linear",
    time_alignment=True,
):
    time_ref = df_l[time_l_label].values
    x_align_raw, y_align_raw = (df_r[time_r_label].values, df_r[values_r_label].values)
    not_null = pd.notnull(time_ref)

    if time_alignment:
        dsp = dsp_sync.register()

        data = {}
        time_ref = time_ref[not_null]
        velocity_ref = df_l[values_l_label].values
        velocity_ref = velocity_ref[not_null]
        data["ref"] = dict(time=time_ref, velocity=velocity_ref)
        select = np.isfinite(x_align_raw) & np.isfinite(y_align_raw)
        x_align, y_align = (x_align_raw[select], y_align_raw[select])

        data["align"] = dict(time=x_align, velocity=y_align)

        sol = dsp(
            dict(
                data=data,
                x_label="time",
                y_label="velocity",
                reference_name="ref",
                interpolation_method="cubic",
            )
        )
        time_offset = [-sol["shifts"]["align"]]
    else:
        time_offset = 0

    df_r[time_r_label] = df_r[time_r_label] + time_offset

    x_align_col_raw = x_align_raw
    for col in [col for col in df_r.columns if col != "time_ref"]:
        y_align_col_raw = df_r[col].values
        select = np.isfinite(x_align_col_raw) & np.isfinite(y_align_col_raw)
        x_align_col, y_align_col = (x_align_col_raw[select], y_align_col_raw[select])
        if len(x_align_col) > 0:
            df_l.loc[not_null, col] = interp1d(
                x_align_col,
                y_align_col,
                bounds_error=False,
                fill_value=np.nan,
                kind=interpolation_method,
            )(time_ref)
        else:
            None
    return df_l.copy()
