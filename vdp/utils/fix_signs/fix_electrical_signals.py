import numpy as np
import pandas as pd


def fix_battery_current(V, I):
    """
    Parameters
    ----------
    V : float
        Voltage [V].
    I : float
        Current [A].

    Returns
    -------
    unknown
        tuple (Voltage [V], Current [A]).
    """
    V = np.abs(V)

    circuit_closed = V > 0.5 * np.percentile(V[pd.notnull(V)], 50)
    I_new = I[circuit_closed]
    V_new = V[circuit_closed]

    not_null = pd.notnull(I_new) & pd.notnull(V_new)
    I_new = I_new[not_null]
    V_new = V_new[not_null]

    m, c = np.polyfit(I_new, V_new, 1)
    if m > 0:
        return (V, I)
    else:
        return (V, -I)


def fix_motor_current(V, I):
    """
    Parameters
    ----------
    V : float
        Voltage [V].
    I : float
        Current [A].

    Returns
    -------
    unknown
        Current [A].
    """

    circuit_closed = V > 0.5 * np.percentile(V, 50)
    I_new = I[circuit_closed]
    V_new = V[circuit_closed]
    m, c = np.polyfit(I_new, V_new, 1)
    if m < 0:
        return I
    else:
        return -I


def fix_consumers_currents(ts_dict):

    fixed_ts_dict = {}

    for key, current_ts in zip(ts_dict.keys(), ts_dict.values()):
        fixed_ts_dict[key] = current_ts * np.sign(np.mean(current_ts))
    return fixed_ts_dict


if __name__ == "__main__":
    # V = np.array([-400, -401, -399, -380, -380, -380])
    # I = -np.array([10, 9, 11, -10, -9, -11])
    # V_new, I_new = fix_battery_current(V, I)
    # import matplotlib.pyplot as plt
    # plt.figure()
    # plt.scatter(I, V, label='old')
    # plt.scatter(I, -V, label='old_fix')
    # plt.scatter(I_new, V_new, label='new')
    # plt.legend()
    # plt.show()

    dict_to_fix = {
        "I_COOLER": np.array([-10, -20]),
        "I_HEATER": np.array([10, 20]),
        "I_DCDC": np.array([-2, -2.1]),
    }

    print("To fix: ", dict_to_fix)

    fixed_dict = fix_consumers_currents(dict_to_fix)

    print("Fixed: ", fixed_dict)

    print(fixed_dict)
