import datetime
import numpy as np
import pandas as pd
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
from pydantic import BaseModel
import cantools
from typing import List, Union


# TODO: comment class
class Signal(BaseModel):
    """
    A class representing a signal.

    Parameters:
    -----------
    name : str
        The name of the signal.
    um : str, optional
        The unit of measurement for the signal. Default is an empty string.
    t : List[Union[int, float]], optional
        The list of time values for the signal observations. Default is an empty list.
    v : List[Union[int, float, str, bool]], optional
        The list of signal values. Default is an empty list.

    Examples:
    ---------
    >>> signal = Signal(name="Temperature", um="Celsius")
    >>> signal.add_observation(t=0, v=25)
    >>> signal.add_observation(t=1, v=26)
    >>> signal.add_observation(t=2, v=27)
    >>> signal.interpolate(new_time=1.5)
    26.5
    >>> signal.plot()

    Methods:
    --------
    add_observation(t: Union[int, float], v: Union[int, float, str, bool])
        Add a new observation to the signal.
    return_last_categorical(new_t: Union[int, float]) -> str
        Return the last categorical value of the signal at a given time.
    interpolate(new_time: Union[int, float], kind='previous') -> Union[int, float, str]
        Interpolate the signal value at a given time using the specified interpolation method.
    plot()
        Plot the signal observations.
    """

    name: str
    um: str = ""
    t: List[Union[int, float]] = []
    v: List[Union[int, float, str, bool]] = []

    def add_observation(self, t: Union[int, float], v: Union[int, float, str, bool]):
        if (
            np.isscalar(t)
            or isinstance(t, datetime.datetime)
            or isinstance(t, pd._libs.tslibs.timestamps.Timestamp)
        ):
            self.t += [t]
        elif isinstance(t, np.datetime64):
            if len(t.ravel()) > 1:
                self.t += list(t)
            else:
                self.t += [t]
        else:
            self.t += [t]

        if type(v) == cantools.database.namedsignalvalue.NamedSignalValue:
            self.v += [v.value]
        elif isinstance(v, np.datetime64):
            if len(v.ravel()) > 1:
                self.v += list(v)
            else:
                self.v += [v]
        else:
            self.v += [v]

    def return_last_categorical(self, new_t: Union[int, float]) -> str:
        if (new_t > np.nanmin(self.t)) and (new_t < np.nanmax(self.t)):
            return np.array(self.v)[np.array(self.t) <= new_t][-1]
        else:
            return ""

    def interpolate(
        self, new_time: Union[int, float], kind="previous"
    ) -> Union[int, float, str]:

        if type(self.v[0]) == str:
            return np.vectorize(self.return_last_categorical, otypes=[str])(new_time)
        else:
            return interp1d(
                self.t,
                self.v,
                bounds_error=False,
                fill_value=(np.nan, np.nan),
                kind=kind,
            )(new_time)

    def plot(self):
        min_len = min(len(self.t), len(self.v))
        x = self.t[:min_len]
        y = self.v[:min_len]
        plt.figure()
        plt.plot(x, y)
        plt.show()  # , fill_value=(self.v[0], self.v[-1]), kind=kind)(new_time)
