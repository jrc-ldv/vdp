dict_defaults = {
    "replace_ice_motoring_torque_with_default": {"value": 0, "initial_dist": 0},
    "fuel_type": {"value": "gasoline", "initial_dist": 0},
    "mass_ratio_coc": {"value": 1, "initial_dist": 2},
    "f2_temp_ratio": {"value": 1, "initial_dist": 2},
    "inertia_factor": {"value": 1.03, "initial_dist": 2},
}
