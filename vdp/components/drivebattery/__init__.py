import numpy as np
import pandas as pd
import schedula as sh
from vdp.physics import calc_P_el_from_V_I, calc_V_from_P_el_I
from vdp.algorithms import calc_cumulative
from vdp.algorithms import calculate_trusted_values_in_discretised_signal
import statsmodels.api as sm


def hvbat_circuit_closed(V_HVBAT):
    """The function determines if the circuit is closed based on the voltage of the HVBAT.

    Parameters
    ----------
    V_HVBAT: np.ndarray
        V_HVBAT is the voltage of the high-voltage battery.

    Returns
    -------
    np.ndarray
        A boolean value indicating whether the circuit is closed depending on the voltage value V_HVBAT.

    """
    return V_HVBAT > 0.5 * np.nanmedian(V_HVBAT)


def keep_only_circuit_closed(V_HVBAT, X):
    """The function takes a list of voltage values `V_HVBAT` and a list of arrays `X` corresponding to other fields,
    and returns a new list `Y` where each of these arrays contains only the elements corresponding to a closed circuit
    based on the values in `V_HVBAT`. This function is used, for example, to filter data for models calibration.

    Parameters
    ----------
    V_HVBAT: np.ndarray
        The parameter V_HVBAT represents the voltage of the high-voltage battery.
    X: np.ndarray
        X is a list of np.ndarray objects. Each np.ndarray object represents a set of values from other fields related
        to the high-voltage system.

    Returns
    -------
    np.ndarray
        A list of np.ndarray objects sourcing from those in 'X' where the values correspond to a closed circuit
        condition of the high-voltage system.
    """
    Y = []
    select = hvbat_circuit_closed(V_HVBAT)
    for x in X:
        Y += [x[select]]
    return Y


def fill_open_circuit_with_na(V_HVBAT, X):
    """The function takes a list of voltage values `V_HVBAT` and a list of arrays `X` corresponding to other fields,
    and returns a new list `Y` where each of these arrays contains only the elements corresponding to a closed circuit
    based on the values in `V_HVBAT` and nan for open circuit. This function is used, for example, to filter data for
    models calibration.

    Parameters
    ----------
    V_HVBAT: np.ndarray
        The parameter V_HVBAT represents the voltage of the high-voltage battery.
    X: np.ndarray
        X is a list of np.ndarray objects. Each np.ndarray object represents a set of values from other fields related
        to the high-voltage system.

    Returns
    -------
    np.ndarray
        A list of np.ndarray objects sourcing from those in 'X' where the values correspond to a closed circuit
        condition of the high-voltage system.
    """
    Y = []
    select = hvbat_circuit_closed(V_HVBAT)
    for x in X:
        Y += [np.where(select, x, np.nan)]
    return Y


def calc_E_HVBAT_cumulative_OBD_from_CEC_CED(CEC, CED, V_HVBAT):
    """The function calculates the cumulative high-voltage battery energy using OBD (On-Board Diagnostic) parameters
    from CEC (Cumulative Energy Charged) and CED (Cumulative Energy Discharged) values, considering only the closed
    circuit condition of the high-voltage system.

    Parameters
    ----------
    CEC: np.ndarray
        CEC stands for Cumulative Energy Charged, which represents the total energy charged to the battery during the
        observation period.
    CED: np.ndarray
        CED stands for Cumulative Energy Discharged, which represents the total energy discharged from the battery
        during the observation period.
    V_HVBAT: np.ndarray
        V_HVBAT is the voltage of the high-voltage battery.

    Returns
    -------
    np.ndarray
        the cumulative high-voltage battery energy using OBD (On-Board Diagnostic) parameters, i.e. the delta battery
        energy over the observation period.
    """
    CEC_cc, CED_cc = keep_only_circuit_closed(V_HVBAT, [CEC, CED])
    return (np.abs(CED) - np.min(np.abs(CED_cc))) - (
        np.abs(CEC) - np.min(np.abs(CEC_cc))
    )


def calc_E_HVBAT_cumulative_OBD_from_OBD_residual_energy(E_RES_HVBAT, V_HVBAT):
    """The function calculates the cumulative high-voltage battery energy from OBD (On-Board Diagnostic) based on the
    residual energy, considering only the closed circuit condition of the high-voltage system.

    Parameters
    ----------
    E_RES_HVBAT: np.ndarray
        The parameter E_RES_HVBAT represents the residual energy of the high-voltage battery (kWh).
    V_HVBAT: np.ndarray
        The parameter V_HVBAT represents the voltage of the high-voltage battery (V).

    Returns
    -------
    np.ndarray
        The cumulative high-voltage battery energy based on the OBD residual energy (kWh).
    """
    E_RES_HVBAT_cc = keep_only_circuit_closed(V_HVBAT, [E_RES_HVBAT])
    return -(
        np.abs(E_RES_HVBAT) - np.max(np.abs(E_RES_HVBAT_cc))
    )  # TODO: evaluate using E_RES_HVBAT[0] instead of max


def def_V_HV_from_V_HVBAT(V_HVBAT):
    """The function def_V_HV_from_V_HVBAT defines the voltage level of the high-voltage system based on the high-voltage
    battery voltage.

    Parameters
    ----------
    V_HVBAT: np.ndarray
        The voltage of the high-voltage battery in (V).

    Returns
    -------
    np.ndarray
        The high-voltage system voltage in (V).
    """
    return V_HVBAT


def def_V_HVBAT_from_V_HV(V_HV):
    """The function def_V_HVBAT_from_V_HV takes defines the voltage of the high-voltage battery based on the high-voltage
    system voltage.

    Parameters
    ----------
    V_HV: np.ndarray
        The high-voltage system voltage in (V).

    Returns
    -------
    np.ndarray
        The high-voltage battery voltage in (V).

    """

    return V_HV


def calc_P_HVBAT_from_V_HV_I_HVBAT(V_HVBAT, I_HVBAT):
    """The function calculates the high-voltage battery electrical power from its voltage and current.

    Parameters
    ----------
    V_HVBAT: np.ndarray
        The voltage of the high-voltage battery in volts (V).
    I_HVBAT: np.ndarray
        The current of the high-voltage battery in amperes (A).

    Returns
    -------
    np.ndarray
        The high-voltage battery power in kilo-watts (kW).

    """
    return calc_P_el_from_V_I(V_HVBAT, I_HVBAT)


def calc_V_HVBAT_from_P_HVBAT_I_HVBAT(P_HVBAT, I_HVBAT):
    """The function calculates the high-voltage battery voltage from its power and current.

    Parameters
    ----------
    P_HVBAT: np.ndarray
        The power of the high-voltage battery in kilo-watts (kW).
    I_HVBAT: np.ndarray
        The current of the high-voltage battery in amperes (A).

    Returns
    -------
    np.ndarray
        The voltage of the high-voltage battery in volts (V).

    """
    return calc_V_from_P_el_I(P_HVBAT, I_HVBAT)


def calc_I_HVBAT_from_P_HVBAT_V_HV(P_HVBAT, V_HV):
    """The function calculates the high-voltage battery current from its power and voltage.

    Parameters
    ----------
    P_HVBAT: np.ndarray
        The power output of the high-voltage battery (in watts).
    V_HV: np.ndarray
        The voltage of the high-voltage battery (V_HV) in volts.

    Returns
    -------
    np.ndarray
        The current in the high voltage system in amperes (A).
    """
    return calc_V_from_P_el_I(P_HVBAT, V_HV)


def calc_E_HVBAT_cumulative_from_SOC_HVBAT(
    V_HVBAT, V_HVBAT_nominal, SOC_HVBAT, HVBAT_capacity
):
    """The function calculates the cumulative energy change of the high-voltage battery based on its state of
    charge, capacity and voltage.

    Parameters
    ----------
    V_HVBAT: np.ndarray
        The voltage of the high-voltage battery.
    V_HVBAT_nominal: float
        The nominal voltage of the high-voltage battery is the expected or rated voltage
        of the battery.
    SOC_HVBAT: np.ndarray
        The State of Charge (SOC) of the High Voltage Battery (%). It represents the
    amount of energy stored in the battery as a percentage of its total capacity.
    HVBAT_capacity: float
        HVBAT_capacity is the capacity of the high-voltage battery in ampere-hours (Ah).

    Returns
    -------
    np.ndarray
        the cumulative energy change of the high-voltage battery based on the state of charge of the high-voltage battery.
    """
    SOC_HVBAT_cc = keep_only_circuit_closed(V_HVBAT, [SOC_HVBAT])[0]
    return np.where(
        hvbat_circuit_closed(V_HVBAT),
        -(SOC_HVBAT - SOC_HVBAT_cc[0]) / 100 * HVBAT_capacity * V_HVBAT_nominal / 1000,
        0,
    )


def calc_drive_battery_slope_intercept_from_V_I(V_HVBAT, I_HVBAT):
    """The function performs a linear regression over the drive battery current and voltage values and returns the
    slope and intercept.

    Parameters
    ----------
    V_HVBAT: np.ndarray, pd.Series
        The voltage of the high-voltage battery.
    I_HVBAT: np.ndarray, pd.Series
        The current of the high-voltage battery in amperes (A).

    Returns
    -------
    tuple: the slope and intercept of the linear regression over the drive battery current and voltage values.

    """
    V_HVBAT = np.array(V_HVBAT)
    I_HVBAT = np.array(I_HVBAT)
    V, I = keep_only_circuit_closed(V_HVBAT, [V_HVBAT, I_HVBAT])
    X = I
    X = sm.add_constant(X)
    model = sm.OLS(V, X)  # linear fitting on V_HVBAT and I_HVBAT values
    results = model.fit()
    return results.params[1], results.params[0]


def recalculate_SOC_coulomb_counting(TIME, I_HVBAT, V_HVBAT, SOC_HVBAT, HVBAT_capacity):
    SOC_HVBAT_cc = keep_only_circuit_closed(V_HVBAT, [SOC_HVBAT])[0]
    return (
        SOC_HVBAT_cc[0] + calc_cumulative(TIME, I_HVBAT) / 3600 / (HVBAT_capacity) * 100
    )


def calculate_continuous_SOC_from_discretised_SOC(
    TIME, I_HVBAT, V_HVBAT, SOC_HVBAT, SOC_HVBAT_recalculated
):

    I_abs_cum = calc_cumulative(TIME, np.abs(I_HVBAT)) / 3600

    SOC_HVBAT_back_filled = (
        pd.Series(fill_open_circuit_with_na(V_HVBAT, [SOC_HVBAT])[0]).bfill().values
    )

    (
        time_real,
        soc_real,
        SOC_changed_value,
        SOC_changed_value_i,
    ) = calculate_trusted_values_in_discretised_signal(TIME, SOC_HVBAT_back_filled)

    SOC_continuous = np.zeros_like(SOC_HVBAT)

    SOC_rec_adj = SOC_HVBAT_recalculated - (
        SOC_HVBAT_recalculated[SOC_changed_value_i[0]] - soc_real[0]
    )

    for i in range(SOC_changed_value_i[-1] - SOC_changed_value_i[0] + 1):
        k = i + SOC_changed_value_i[0]

        if k in SOC_changed_value_i:

            SOC_real_i = soc_real[k == SOC_changed_value_i][0]

        else:

            left = SOC_changed_value_i[SOC_changed_value_i < k][-1]
            right = SOC_changed_value_i[SOC_changed_value_i > k][0]

            soc_real_left = soc_real[left == SOC_changed_value_i][0]
            soc_real_right = soc_real[right == SOC_changed_value_i][0]

            I_abs_cum_left = I_abs_cum[left]
            I_abs_cum_right = I_abs_cum[right]

            SOC_rec_left = SOC_rec_adj[left]
            SOC_rec_right = SOC_rec_adj[right]

            delta_SOC_rec = SOC_rec_right - SOC_rec_left
            delta_soc_real = soc_real_right - soc_real_left

            factor = (I_abs_cum[k] - I_abs_cum_left) / (
                I_abs_cum_right - I_abs_cum_left
            )
            factor = factor if np.isfinite(factor) else 1

            SOC_real_i = (
                soc_real_left
                + (SOC_rec_adj[k] - SOC_rec_left)
                - (delta_SOC_rec - delta_soc_real) * factor
            )

            if np.isfinite(SOC_real_i) == False:
                break

        SOC_continuous[k] = SOC_real_i

    for i in reversed(range(SOC_changed_value_i[0])):
        SOC_continuous[i] = SOC_continuous[i + 1] + (
            SOC_rec_adj[i] - SOC_rec_adj[i + 1]
        )

    for i in range(len(SOC_HVBAT) - SOC_changed_value_i[-1]):
        k = i + SOC_changed_value_i[-1]
        SOC_continuous[k] = SOC_continuous[k - 1] + (
            SOC_rec_adj[k] - SOC_rec_adj[k - 1]
        )

    return np.array(SOC_continuous, dtype=float)


# The code block you provided is creating a `Dispatcher` object named `dsp` and adding several
# functions to it.
dsp = sh.Dispatcher(name="dsp")
dsp.add_func(
    calc_E_HVBAT_cumulative_OBD_from_CEC_CED,
    outputs=["E_HVBAT_cumulative_from_OBD"],
    weight=2,
)
dsp.add_func(
    calc_E_HVBAT_cumulative_OBD_from_OBD_residual_energy,
    outputs=["E_HVBAT_cumulative_from_OBD"],
)
dsp.add_func(def_V_HV_from_V_HVBAT, outputs=["V_HV"])
dsp.add_func(def_V_HVBAT_from_V_HV, outputs=["V_HVBAT"])
dsp.add_func(calc_P_HVBAT_from_V_HV_I_HVBAT, outputs=["P_HVBAT"])
dsp.add_func(calc_I_HVBAT_from_P_HVBAT_V_HV, outputs=["I_HVBAT"])
dsp.add_func(calc_V_HVBAT_from_P_HVBAT_I_HVBAT, outputs=["V_HVBAT"])
dsp.add_func(
    calc_E_HVBAT_cumulative_from_SOC_HVBAT,
    outputs=["E_HVBAT_cumulative_from_SOC_HVBAT"],
)
dsp.add_func(
    calc_drive_battery_slope_intercept_from_V_I,
    outputs=["drive_battery_slope", "drive_battery_intercept"],
)
dsp.add_func(recalculate_SOC_coulomb_counting, outputs=["SOC_HVBAT_recalculated"])
dsp.add_func(
    calculate_continuous_SOC_from_discretised_SOC, outputs=["SOC_HVBAT_continuous"]
)

if __name__ == "__main__":
    # The `dsp.plot()` function is used to visualise the computation graph of the `Dispatcher` object
    # `dsp`. It shows the flow of data between the input and output variables, as well as the
    # functions that are applied to the data. This can help in understanding the structure and
    # dependencies of the computation.
    dsp.plot()
    # The code `sol = dsp.dispatch(inputs={...})` is using the `Dispatcher` object `dsp` to perform
    # the computation based on the provided inputs. The inputs are passed as a dictionary where the
    # keys are the names of the input variables and the values are the corresponding arrays of values.
    sol = dsp.dispatch(
        inputs={
            "V_HV": np.array([400, 402, 399]),
            "P_HVBAT": np.array([4, 3.8, 4.1]),
            "I_MAC": np.array([0.1, 0.2, 0.3]),
            "E_RES_HVBAT": np.array([2, 1.9, 1.7]),
            "CEC": np.array([0.1, 0.1, 0.2]),
            "CED": np.array([1, 1.1, 1.3]),
        },
    )
    sol.plot()
