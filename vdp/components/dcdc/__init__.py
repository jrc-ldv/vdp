import numpy as np
import schedula as sh
from vdp.physics import calc_P_el_from_V_I, calc_V_from_P_el_I


def def_P_DCDC_HV_from_user_input(TIME, P_DCDC_HV_const):
    """The function is used to define a constant DC-DC converter power (high-voltage side) from user input.

    Parameters
    ----------
    TIME : np.ndarray
        The array of time values in seconds (s).
    P_DCDC_HV_const: int, float
        The power to be assumed on the high-voltage DC-DC converter (high-voltage side) in kilowatts (kW).

    Returns
    -------
    np.ndarray
        DC-DC converter power (high-voltage side).

    """
    return np.ones_like(TIME) * P_DCDC_HV_const


def calc_P_DCDC_HV_from_V_HV_I_DCDC_HV(V_HV, I_DCDC_HV):
    """The function calculates the power of a high voltage DC-DC converter based on the input voltage and
    current.

    Parameters
    ----------
    V_HV: np.ndarray, float
        The high voltage system voltage in volts (V).
    I_DCDC_HV: np.ndarray, float
        The current flowing through the DC-DC converter (high-voltage side) in amperes (A).

    Returns
    -------
    np.ndarray, float
        The power (kW) of the DC-DC converter (high-voltage side).

    """
    return calc_P_el_from_V_I(V_HV, I_DCDC_HV)


def calc_I_DCDC_HV_from_P_DCDC_HV_V_HV(P_DCDC_HV, V_HV):
    """The function calculates the current (I_DCDC_HV) on the high-voltage side of the DC-DC converter based on the power
    and voltage inputs.

    Parameters
    ----------
    P_DCDC_HV: np.ndarray, float
        The power of the DC-DC converter (high-voltage side) in kilowatts (kW).
    V_HV: np.ndarray, float
        The voltage of the DC-DC converter (high-voltage side) in volts (V).

    Returns
    -------
    np.ndarray, float
        The current of the DC-DC converter (high-voltage side) in amperes (A).
    """
    return calc_V_from_P_el_I(P_DCDC_HV, V_HV)


def calc_P_DCDC_LV_from_V_LV_I_DCDC_LV(V_LV, I_DCDC_LV):
    """The function calculates the power on the low-voltage side of the DC-DC converter based on the voltage and
    current inputs.

    Parameters
    ----------
    V_LV: np.ndarray, float
        The voltage of the DC-DC converter (low-voltage side) in volts (V).
    I_DCDC_LV: np.ndarray, float
        The current of the DC-DC converter (low-voltage side) in amperes (A).

    Returns
    -------
        The power (kW) of the DC-DC converter (low-voltage side).

    """
    return calc_P_el_from_V_I(V_LV, I_DCDC_LV)


def calc_I_DCDC_LV_from_P_DCDC_LV_V_LV(P_DCDC_LV, V_LV):
    """The function calculates the current (I_DCDC_LV) flowing through the DC-DC converter based
    on the power (P_DCDC_LV) and the voltage (V_LV).

    Parameters
    ----------
    P_DCDC_LV: np.ndarray, float
        The power output of the DC-DC converter (low-voltage side) in watts (W).
    V_LV: np.ndarray, float
        The voltage of the DC-DC converter (low-voltage side) in volts (V).

    Returns
    -------
    npo.ndarray, float
        The current of the DC-DC converter (low-voltage side) in amperes (A).

    """
    return calc_V_from_P_el_I(P_DCDC_LV, V_LV)


def calc_P_DCDC_HV_from_P_DCDC_LV(P_DCDC_LV, DCDC_efficiency=0.925):
    """The function calculates the power at the high voltage side of the DC-DC converter given the power at
    the low voltage side and the efficiency of the converter.

    Parameters
    ----------
    P_DCDC_LV: np.ndarray, float
        The power of the DC-DC converter (low-voltage side) in kilowatts (kW).
    DCDC_efficiency: np.ndarray, float
        The DCDC_efficiency parameter represents the efficiency of the DC-DC converter. It is a decimal
        value between 0 and 1, where 1 represents 100% efficiency.

    Returns
    -------
    np.ndarray, float
        The power (kW) of the DC-DC converter (high-voltage side).

    """

    return np.abs(P_DCDC_LV / DCDC_efficiency)


def calc_P_DCDC_LV_from_P_DCDC_HV(P_DCDC_HV, DCDC_efficiency=0.925):
    """The function calculates the power at the low voltage side of the DC-DC converter given the power at
    the high voltage side and the efficiency of the converter.

    Parameters
    ----------
    P_DCDC_HV: np.ndarray, float
        The power of the DC-DC converter (high-voltage side) in kilowatts (kW).
    DCDC_efficiency: np.ndarray, float
        The efficiency of the DC-DC converter, which is the ratio of the output power to the input power.
    It is typically given as a decimal value between 0 and 1.

    Returns
    -------
    np.ndarray, float
        The power of the DC-DC converter (low-voltage side) in kilowatts (kW).

    """
    return -np.abs(P_DCDC_HV * DCDC_efficiency)


def def_V_LV():
    """The function returns a generic voltage value (14 volts) for the low-voltage system when not provided.

    Returns
    -------
        the generic voltage of the low-voltage system.

    """
    return 14


# The code block you provided is creating a `Dispatcher` object named `dsp` and adding several
# functions to it.
dsp = sh.Dispatcher(name="dsp")
dsp.add_func(def_P_DCDC_HV_from_user_input, outputs=["P_DCDC_HV"])
dsp.add_func(calc_I_DCDC_LV_from_P_DCDC_LV_V_LV, outputs=["I_DCDC_LV"])
dsp.add_func(calc_P_DCDC_LV_from_P_DCDC_HV, outputs=["P_DCDC_LV"])
dsp.add_func(calc_I_DCDC_HV_from_P_DCDC_HV_V_HV, outputs=["I_DCDC_HV"])
dsp.add_func(calc_P_DCDC_HV_from_P_DCDC_LV, outputs=["P_DCDC_HV"], weight=1)
dsp.add_func(calc_P_DCDC_LV_from_V_LV_I_DCDC_LV, outputs=["P_DCDC_LV"])
dsp.add_func(calc_P_DCDC_HV_from_V_HV_I_DCDC_HV, outputs=["P_DCDC_HV"])
dsp.add_func(def_V_LV, outputs=["V_LV"])

if __name__ == "__main__":
    # The code `dsp.plot()` is used to plot the dependency graph of the functions added to the
    # `Dispatcher` object `dsp`. This graph shows the relationships between the input and output variables
    # of the functions.
    dsp.plot()
    sol = dsp.dispatch(
        inputs={
            "V_HV": np.array([400, 402, 399]),
            "V_LV": np.array([13, 13, 13.1]),
            "I_DCDC_LV": np.array([41, 42.8, 42.1]),
            "I_DCDC_HV": np.array([1, 2.8, 2.1]),
        }
    )
    sol.plot()
