import numpy as np
import schedula as sh


def initialise_eff_ice_to_ems_for_evs(veh_type):
    if veh_type == "ev":
        return 0


def calc_ice_motoring_torque(
    fuel_rate_g_s,
    ice_speed,
    ice_idling_speed,
    ice_max_speed,
    ice_max_torque,
    clutch_open,
):
    """The function calculates the motoring torque of an internal combustion engine (ICE) based on the fuel
    rate, engine speed, idling speed, maximum speed, maximum torque, and clutch status.

    Parameters
    ----------
    ice_fuel_rate_g_s: np.ndarray
        The fuel rate of the internal combustion engine (ICE) in grams per second (g/s).
    ice_speed: np.ndarray
        The current speed of the internal combustion engine (ICE) in revolutions per minute (RPM).
    ice_idling_speed: np.ndarray
        The idle speed of the internal combustion engine (ICE) in revolutions per minute (RPM).
    ice_max_speed: np.ndarray
        The maximum speed of the internal combustion engine (ICE) in revolutions per minute (RPM).
    ice_max_torque: np.ndarray
        The maximum torque that the internal combustion engine (ICE) can produce.
    clutch_open: np.ndarray
        A boolean value indicating whether the clutch is open or not.

    Returns
    -------
    np.ndarray
        The motoring torque of an internal combustion engine (ICE) based on the given parameters.
    """
    from scipy.interpolate import interp1d

    t_idl_spd = -0.07 * ice_max_torque * 0.8
    t_max_spd = -0.1 * ice_max_torque * 0.8

    f = interp1d(
        [ice_idling_speed, ice_max_speed],
        [t_idl_spd, t_max_spd],
        bounds_error=False,
        fill_value="extrapolate",
    )
    t_motoring_raw = np.where(fuel_rate_g_s <= 0.01, f(ice_speed), 0)
    return np.where(clutch_open, 0, t_motoring_raw)


def def_ice_reference_torque_from_const(ice_reference_torque_const):
    """The function returns the ice reference torque constant.

    Parameters
    ----------
    ice_reference_torque_const: np.ndarray
        The constant value of the ice reference torque.

    Returns
    -------
    np.ndarray
        The constant value of the ice reference torque.

    """
    return ice_reference_torque_const


def calc_ice_torque_from_OBD(
    actual_ice_torque_percent, ice_friction_percent, ice_reference_torque
):
    """The function calculates the ice torque based on the actual ice torque percentage, ice friction
    percentage, and ice reference torque.

    Parameters
    ----------
    actual_ice_torque_percent: np.ndarray
        The actual torque output of the internal combustion engine (ICE) as a percentage of its maximum
        torque output.
    ice_friction_percent: np.ndarray
        The ice_friction_percent parameter represents the percentage of frictional losses in the internal
        combustion engine (ICE). It is a measure of the power lost due to friction within the engine
        components.
    ice_reference_torque: np.ndarray
        The ice_reference_torque is the maximum torque that the internal combustion engine (ICE) can
        produce.

    Returns
    -------
    np.ndarray
        The calculated ice torque based on the actual ice torque percentage, ice friction percentage, and
        ice reference torque.
    """
    return (
        (actual_ice_torque_percent - ice_friction_percent) / 100 * ice_reference_torque
    )


def calc_ice_power_from_OBD(ice_speed, ice_torque_from_obd):
    """The function calculates the power of an internal combustion engine (ICE) based on the speed and
    torque obtained from an On-Board Diagnostics (OBD) system.

    Parameters
    ----------
    ice_speed: np.ndarray
        The speed of the internal combustion engine (ICE) in revolutions per minute (RPM).
    ice_torque_from_obd: np.ndarray
        The torque value of the internal combustion engine (ICE) obtained from the On-Board Diagnostics
        (OBD) system.

    Returns
    -------
    np.ndarray
        The power of an internal combustion engine (ICE) based on the speed and torque obtained from an
        On-Board Diagnostics (OBD) system.

    """
    return calc_ice_power(ice_speed, ice_torque_from_obd)


def def_ice_torque_out_from_OBD(
    ice_torque_from_obd,
    ice_motoring_torque,
    fuel_rate_g_s,
    replace_ice_motoring_torque_with_default,
):
    """The function calculates the ice torque output based on the ice torque obtained from the On-Board
    Diagnostics (OBD) system, the motoring torque, the fuel rate, and a flag that determines whether to
    replace the motoring torque with a default value or not.

    Parameters
    ----------
    ice_torque_from_obd: np.ndarray
        The torque value obtained from the OBD (On-Board Diagnostics) system for the internal combustion
        engine (ICE).
    ice_motoring_torque: np.ndarray
        The ice_motoring_torque parameter represents the torque generated by the internal combustion engine
        (ICE) when it is operating in motoring mode.
    ice_fuel_rate_g_s: np.ndarray
        The ice_fuel_rate_g_s parameter represents the fuel rate of the internal combustion engine (ICE) in
        grams per second.
    replace_ice_motoring_torque_with_default: np.ndarray
        The parameter is a flag that determines whether to replace the motoring torque with a default value
        or not. If the value is 1, the motoring torque is replaced with a default value. If the value is 0,
        the motoring torque is not replaced.

    Returns
    -------
    np.ndarray
        The ice torque output based on the ice torque obtained from the On-Board Diagnostics (OBD) system,
        the motoring torque, the fuel rate, and a flag that determines whether to replace the motoring
        torque with a default value or not.

    """
    if replace_ice_motoring_torque_with_default == 1:
        if fuel_rate_g_s.any():
            return np.where(fuel_rate_g_s > 0, ice_torque_from_obd, ice_motoring_torque)
        else:
            return False
    else:
        return ice_torque_from_obd


def calc_ice_power(ice_speed, ice_torque_out):
    """The function calculates the power output of an internal combustion engine (ICE) based on its speed
    and torque.

    Parameters
    ----------
    ice_speed: np.ndarray
        The speed of the internal combustion engine (ICE) in revolutions per minute (RPM).
    ice_torque_out: np.ndarray
        The torque output of the internal combustion engine (ICE) in Newton-meters (Nm).

    Returns
    -------
    np.ndarray
        The power output of an internal combustion engine (ICE) based on its speed and torque.
    """
    return ice_speed * ice_torque_out * 2 * np.pi / 60 / 1000


def calc_ice_torque_neg(ice_torque_out):
    """The function calculates the negative torque output of an internal combustion engine (ICE) based on
    its torque output.

    Parameters
    ----------
    ice_torque_out: np.ndarray
        The torque output of an internal combustion engine (ICE).

    Returns
    -------
    np.ndarray
        The negative torque output of an internal combustion engine (ICE) based on its torque output.
    """
    return np.where(ice_torque_out < 0, ice_torque_out, 0)


def calc_ice_torque_neg_from_default_motoring_model(ice_motoring_torque):
    """The function returns the input torque value for an internal combustion engine in a motoring mode.

    Parameters
    ----------
    ice_motoring_torque: np.ndarray
        The ice_motoring_torque parameter represents the torque generated by an internal combustion engine
        (ICE) during motoring operation.

    Returns
    -------
    np.ndarray
        The input torque value for an internal combustion engine in a motoring mode.
    """
    return ice_motoring_torque


def calc_ice_power_neg(ice_speed, ice_torque_neg):
    """The function calculates the negative power output of an internal combustion engine (ICE) based on
    the ice_speed and ice_torque_neg parameters.

    Parameters
    ----------
    ice_speed: np.ndarray
        The speed of the internal combustion engine (ICE) in revolutions per minute (RPM).
    ice_torque_neg: np.ndarray
        The negative torque produced by the internal combustion engine (ICE) in Newton-meters (Nm).

    Returns
    -------
    np.ndarray
        The negative power output of an internal combustion engine (ICE) based on the ice_speed and
        ice_torque_neg parameters.
    """
    return ice_speed * ice_torque_neg * 2 * np.pi / 60 / 1000


def def_ice_power_and_speed_zero_for_non_ice_vehicles(TIME, veh_type):
    """The function sets the ice power and speed to zero for non-ice vehicles.

    Parameters
    ----------
    TIME: np.ndarray
        The time duration for which the ice power and speed are calculated for non-ice vehicles.
    veh_type: string
        Represents the type of vehicle. It can be either "ev" for electric vehicles or "fcev" for
        fuel cell electric vehicles.

    Returns
    -------
    np.ndarray
        If the vehicle type is "ev" or "fcev", the function returns two arrays filled with zeros, each with
        the same shape as the input array TIME. Otherwise, it returns the value sh.NONE for both the ice
        power and speed.

    """
    if (veh_type == "ev") or (veh_type == "fcev"):
        return np.zeros_like(TIME), np.zeros_like(TIME)
    else:
        return sh.NONE, sh.NONE


# The above code is creating a dispatcher object named "dsp" and adding several functions to it. Each
# function is associated with specific outputs. The functions are used to calculate various values
# related to ice (internal combustion engine) torque, power, and speed. Some functions have weights
# assigned to them, which determine their priority when multiple functions are available to calculate
# the same output.
dsp = sh.Dispatcher(name="dsp")
dsp.add_func(initialise_eff_ice_to_ems_for_evs, outputs=["eff_ice_to_ems"])
dsp.add_func(calc_ice_motoring_torque, outputs=["ice_motoring_torque"])
dsp.add_func(def_ice_reference_torque_from_const, outputs=["ice_reference_torque"])
dsp.add_func(calc_ice_torque_from_OBD, outputs=["ice_torque_from_obd"])
dsp.add_func(calc_ice_power_from_OBD, outputs=["ice_power_from_obd"])
dsp.add_func(def_ice_torque_out_from_OBD, outputs=["ice_torque_out"])
dsp.add_func(calc_ice_power, outputs=["ice_power_out"])
dsp.add_func(calc_ice_torque_neg, outputs=["ice_torque_neg"])
dsp.add_func(
    calc_ice_torque_neg_from_default_motoring_model,
    outputs=["ice_torque_neg"],
    weight=10,
)
dsp.add_func(calc_ice_power_neg, outputs=["ice_power_neg"])
dsp.add_func(
    def_ice_power_and_speed_zero_for_non_ice_vehicles,
    outputs=["ice_power_out", "ice_speed"],
    weight=20,
)

if __name__ == "__main__":
    dsp.plot()
