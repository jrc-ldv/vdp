import numpy as np
import schedula as sh


# TODO: create documentation for function below
def define_gearbox_upstream_speed(ice_speed):
    # TODO: implement other cases where gearbox_upstrem_speed sources from inputs other than ice_speed (e.g. p2 motor)
    return ice_speed


def identify_gears(TIME, SPEED, ACCELERATION, gearbox_upstream_speed):
    """
    Identify the gears and clutch open status based on the provided inputs.

    Parameters:
    -----------
    TIME: np.ndarray
        Array of time values.
    SPEED: np.ndarray
        The vehicle speed in (km/h).
    ACCELERATION: np.ndarray
        The acceleration values for each time step. It is a list or array of numerical values
        that indicate the rate of change of speed over time in (m/s^2)
    ice_speed: np.ndarray
        The engine speed output in revolutions per minute (RPM).

    Returns:
    --------
    gears: np.ndarray
        Array of identified gears.
    clutch_open: np.ndarray
        Array of clutch open status.

    Notes:
    ------
    - This function uses the CO2MPAS physical model to identify gears and clutch open status.
    - The gear_box_type is set to "automatic" by default.
    - The function is based on:
        [1] [The development and validation of a vehicle simulator for the introduction of Worldwide Harmonized
        test protocol in the European light duty
        vehicle CO2 certification process](https://www.sciencedirect.com/science/article/pii/S030626191830878X)

    Examples:
    ---------
    >>> TIME = [1, 2, 3, 4, 5]
    >>> SPEED = [10, 20, 30, 40, 50]
    >>> ACCELERATION = [1, 2, 3, 4, 5]
    >>> ice_speed = [1000, 2000, 3000, 4000, 5000]
    >>> gears, clutch_open = identify_gears(TIME, SPEED, ACCELERATION, gearbox_upstream_speed)
    >>> print(gears)
    [1, 2, 3, 4, 5]
    >>> print(clutch_open)
    [0, 0, 0, 0, 0]
    """

    from co2mpas.core.model.physical import dsp as dsp_co2mpas_physical

    dsp_co2mpas_physical = dsp_co2mpas_physical.register()

    sol_co2mpas_physical = dsp_co2mpas_physical.dispatch(
        inputs={
            "velocities": SPEED,
            "engine_speeds_out": gearbox_upstream_speed,
            "accelerations": ACCELERATION,
            "times": TIME,
            "gear_box_type": "automatic",
        }
    )

    if "gears" in sol_co2mpas_physical:
        gears = sol_co2mpas_physical["gears"]
        clutch_phases_neg = sol_co2mpas_physical["clutch_phases"]
    else:
        # vehicles with no gears
        gears = np.ones_like(gearbox_upstream_speed)
        clutch_phases_neg = np.array(np.ones_like(gearbox_upstream_speed), dtype=bool)
    clutch_open = np.where(SPEED < 5, True, clutch_phases_neg)

    return gears, clutch_open


# Create a new instance of the Dispatcher class and assign it to the variable 'dsp'
dsp = sh.Dispatcher(name="dsp")

# Add the 'identify_gears' function as a task to the dispatcher 'dsp'
# The outputs of the function are 'gears' and 'clutch_open'
dsp.add_func(define_gearbox_upstream_speed, outputs=["gearbox_upstream_speed"])
dsp.add_func(identify_gears, outputs=["gears", "clutch_open"])

if __name__ == "__main__":
    dsp.plot()
