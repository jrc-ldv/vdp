import numpy as np
import schedula as sh


# TODO: Review the function signature and documentation
def def_P_HVLOADS_from_user_input(TIME, P_HVLOADS_const):
    """The function returns an array of the same length as time, with each element equal to
    P_HVLOADS_const.

    Parameters
    ----------
    TIME: np.ndarray
        The array of time values (s).
    P_HVLOADS_const: float
        The power consumption of the high-voltage loads (kW).

    Returns
    -------
    np.ndarray
        An array of constant power consumption values for high-voltage auxiliaries of the same length as TIME,
        with each element equal to P_HVLOADS_const.
    """
    return np.ones_like(TIME) * P_HVLOADS_const


# TODO: review the function to make it work both for FCEVs and HEVs
def def_P_MAC_from_power_balance(
    P_HVBAT,
    P_EL_MOTOR,
    P_DCDC_HV,
    P_HVLOADS,
    P_FC=None,
    P_EL_MOTOR_FC=None,
    P_DCDC_FC_HV=None,
    P_HVFCLOADS=None,
    veh_type=None,
):
    """The function calculates the power of the mobile air conditioning (MAC) based on the power balance applied on the
    high-voltage system of the vehicle, taking into account the vehicle type. This is seen as the combined power
    consumption of the air conditioning (or heat pump) compressor and electrical cabin heaters (PTC).

    Parameters
    ----------
    P_HVBAT: np.ndarray
        High-voltage battery power (kW). Positive -> charging, negative -> discharging.
    P_EL_MOTOR: np.ndarray
        Electric motor electrical power (KW). Positive -> propulsion, negative -> braking/generating.
    P_DCDC_HV: np.ndarray
        DC-DC converter power (high-voltage side) (kW).
    P_HVLOADS: np.ndarray
        High-voltage loads power consumption (kW), if any.
    P_FC: np.ndarray
        Fuel cell power (kW), if any.
    P_EL_MOTOR_FC: np.ndarray
        Electric motor power in a fuel cell electric vehicle (FCEV), if any.
    P_DCDC_FC_HV: np.ndarray
        Fuel-cell DC-DC converter power (kW).
    P_HVFCLOADS: np.ndarray
        Power consumption of the high-voltage loads in the FC electrical system (kW), if any.
    veh_type: np.ndarray
        The parameter "veh_type" is used to specify the type of vehicle (hev, phev, fcev, ...).
    
    Returns
    -------
    np.ndarray
        the mobile air conditioning high-voltage power consumption (kW).
    """
    if veh_type != "fcev":
        return -(P_HVBAT + P_EL_MOTOR + P_DCDC_HV + P_HVLOADS)
    else:
        return P_FC - (P_EL_MOTOR_FC + P_DCDC_FC_HV + P_HVFCLOADS)


def def_P_EL_MOTOR_from_power_balance(
    P_HVBAT,
    P_MAC,
    P_DCDC_HV,
    P_HVLOADS,
    P_FC=None,
    P_MAC_FC=None,
    P_DCDC_FC_HV=None,
    P_HVFCLOADS=None,
    veh_type=None,
):
    """The function calculates the electrical power of the motor based on the power balance applied on the
    high-voltage system of the vehicle, taking into account the vehicle type.
    
    Parameters
    ----------
    P_HVBAT: np.ndarray
        High-voltage battery power (kW). Positive -> charging, negative -> discharging.
    P_MAC: np.ndarray
        The mobile air conditioning high-voltage power consumption (kW).
    P_DCDC_HV: np.ndarray
        DC-DC converter power (high-voltage side) (kW).
    P_HVLOADS: np.ndarray
        High-voltage loads power consumption (kW), if any.
    P_FC: np.ndarray
        Fuel cell power (kW), if any.
    P_MAC_FC: np.ndarray
        The mobile air conditioning high-voltage power consumption (kW).
   P_DCDC_FC_HV: np.ndarray
        Fuel-cell DC-DC converter power (kW).
    P_HVFCLOADS: np.ndarray
        Power consumption of the high-voltage loads in the FC electrical system (kW), if any.
    veh_type: np.ndarray
        The parameter "veh_type" is used to specify the type of vehicle (hev, phev, fcev, ...)
    
    Returns
    -------
    np.ndarray
        The difference between the power inputs and outputs of the motor system. If the vehicle type is not
        "fcev", it returns the negative sum of the power inputs (P_HVBAT, P_MAC, P_DCDC_HV, P_HVLOADS). If
        the vehicle type is "fcev", it returns the difference between the power input from the fuel cell (P
        _FC) and the sum of the power consumed by the motor air conditioning system (P_MAC_FC), the power
        consumed by the DC-DC converter for the fuel cell system in high voltage mode (P_DCDC_FC_HV), and
        the power consumed by high-voltage fuel cell loads (P_HVFCLOADS).
    """

    if veh_type != "fcev":
        return -(P_HVBAT + P_MAC + P_DCDC_HV + P_HVLOADS)
    else:
        return P_FC - (P_MAC_FC + P_DCDC_FC_HV + P_HVFCLOADS)


def calc_P_HVBAT_reversed_sign(P_HVBAT):
    """The function returns the negative value of the power from the high-voltage battery.

    Parameters
    ----------
    P_HVBAT: np.ndarray
        The parameter P_HVBAT represents the power value of the high-voltage battery.

    Returns
    -------
    np.ndarray
        The negative value of the power from the high-voltage battery.
    """
    return -P_HVBAT


# TODO: Review the function signature and documentation
def def_P_HVOTHERS_from_user_input(TIME, P_HVOTHERS_const):
    """The function returns an array of the same length as time, with each element equal to
    P_HVOTHERS_const.

    Parameters
    ----------
    TIME: np.ndarray
        TIME is a numpy array representing the time values.
    P_HVOTHERS_const: np.ndarray
        The constant value for P_HVOTHERS.

    Returns
    -------
    np.ndarray
        an array of ones with the same shape as the input array `TIME`, multiplied by the constant value
        `P_HVOTHERS_const`.
    """
    return np.ones_like(TIME) * P_HVOTHERS_const


# TODO: Review the function signature and documentation
def def_P_MAC_from_user_input(TIME, P_MAC_const):
    """The function returns an array of the same length as TIME, with each element equal to P_MAC_const.

    Parameters
    ----------
    TIME: np.ndarray
        The time values.
    P_MAC_const: float
        The constant value for P_MAC. It is used to calculate the P_MAC for each time step.

    Returns
    -------
    np.ndarray
        an array of the same size as the input array `TIME`, where each element is equal to the value of
        `P_MAC_const`.

    """
    return np.ones_like(TIME) * P_MAC_const


def calc_P_HVOTHERS(P_HVBAT_rev, P_EL_MOTOR, P_DCDC_HV, P_MAC, P_HVLOADS):
    """The function calculates the remaining power in the high-voltage battery after subtracting the power
    consumed by the electric motor, DC-DC converter, MAC (Main Accessory Controller), and other
    high-voltage loads.

    Parameters
    ----------
    P_HVBAT_rev: np.ndarray
        The power generated by the high-voltage battery in kilo-watts (kW).
    P_EL_MOTOR: np.ndarray
        The power consumed by the electric motor.
    P_DCDC_HV: np.ndarray
        The power consumed by the high-voltage DC-DC converter.
    P_MAC: np.ndarray
        P_MAC refers to the power consumed by the main air compressor in a vehicle.
    P_HVLOADS: np.ndarray
        P_HVLOADS represents the power consumed by other high-voltage loads in the system. This could
        include components such as air conditioning, heating, lighting, and other electrical systems that
        are powered by the high-voltage battery.

    Returns
    -------
    np.ndarray
        The remaining power in the high-voltage battery after subtracting the power consumed by the electric
        motor, DC-DC converter, MAC, and other high-voltage loads.

    """
    return P_HVBAT_rev - (P_EL_MOTOR + P_DCDC_HV + P_MAC + P_HVLOADS)


def validate_EPS_power_balance(
    P_EL_MOTOR,
    P_DCDC_HV,
    P_MAC,
    P_HVLOADS,
    P_FC=None,
    P_EL_MOTOR_FC=None,
    P_DCDC_FC_HV=None,
    P_HVFCLOADS=None,
    veh_type=None,
):
    """The function calculates the power balance for an electric vehicle's
    powertrain system.

    Parameters
    ----------
    P_EL_MOTOR: np.ndarray
        Power consumed by the electric motor.
    P_DCDC_HV: np.ndarray
        Power consumed by the high-voltage DC-DC converter in kilo-watts (kW).
    P_MAC: np.ndarray
        The power consumption of the vehicle's air conditioning system.
    P_HVLOADS: np.ndarray
        The parameter P_HVLOADS represents the power consumed by high-voltage loads in the vehicle.
    P_FC: np.ndarray
        Power generated by the fuel cell (in a fuel cell electric vehicle).
    P_EL_MOTOR_FC: np.ndarray
        The parameter P_EL_MOTOR_FC represents the power consumed by the electric motor in a fuel cell
        electric vehicle (FCEV).
    P_DCDC_FC_HV: np.ndarray
        The parameter P_DCDC_FC_HV represents the power consumed by the high-voltage DC-DC converter in a
        fuel cell electric vehicle (FCEV).
    P_HVFCLOADS: np.ndarray
        The parameter P_HVFCLOADS represents the power consumed by high-voltage fuel cell loads in the
        vehicle.
    veh_type: np.ndarray
        The type of vehicle. It can be "fcev" for fuel cell electric vehicle or any other value for
        non-fuel cell electric vehicle.

    Returns
    -------
    np.ndarray
        the power balance of the electrical power system. If the vehicle type is not "fcev" (fuel cell
        electric vehicle), it returns the sum of P_MAC (power from the main AC source), P_EL_MOTOR (power
        consumed by the electric motor), P_DCDC_HV (power consumed by the high-voltage DC-DC converter), and
        P_HVLOADS (power consumed by high-voltage loads). If the vehicle type is "fcev", it returns the sum
        of P_FC (power generated by the fuel cell), P_EL_MOTOR_FC (power consumed by the electric motor in a
        fuel cell electric vehicle), P_DCDC_FC_HV (power consumed by the high-voltage DC-DC converter in a
        fuel cell electric vehicle), and P_HVFCLOADS (power consumed by high-voltage fuel cell loads).
    """

    if veh_type != "fcev":
        return P_MAC + P_EL_MOTOR + P_DCDC_HV + P_HVLOADS
    else:
        return P_FC - (
            P_EL_MOTOR_FC + P_DCDC_FC_HV + P_HVFCLOADS
        )  # TO BE EXTENDED, THERE IS MORE TO BE ADDED


# The above code is defining a dispatcher object named "dsp" and adding several functions to it. Each
# function takes user input or performs calculations and outputs a specific value. The functions are
# then used to calculate various power values related to high voltage loads, high voltage others, MAC
# (machine), and electric motor. The code also includes a function to validate the power balance of
# the EPS (Electric Power Steering) system.
dsp = sh.Dispatcher(name="dsp")
dsp.add_func(def_P_HVLOADS_from_user_input, outputs=["P_HVLOADS"])
dsp.add_func(def_P_HVOTHERS_from_user_input, outputs=["P_HVOTHERS"])
dsp.add_func(def_P_MAC_from_user_input, outputs=["P_MAC"])
dsp.add_func(def_P_MAC_from_power_balance, outputs=["P_MAC"])
dsp.add_func(def_P_EL_MOTOR_from_power_balance, outputs=["P_EL_MOTOR"], weight=10)
dsp.add_func(calc_P_HVOTHERS, outputs=["P_HVOTHERS"])
dsp.add_func(calc_P_HVBAT_reversed_sign, outputs=["P_HVBAT_rev"])
dsp.add_func(validate_EPS_power_balance, outputs=["P_HVBAT_check"])

if __name__ == "__main__":
    dsp.plot()
    # The above code is creating a dispatch object using the `dsp.dispatch()` function. The dispatch
    # object is being initialized with three input arrays: "V_HV", "P_HVBAT", and "I_MAC". These
    # arrays contain voltage, power, and current values respectively.
    sol = dsp.dispatch(
        inputs={
            "V_HV": np.array([400, 402, 399]),
            "P_HVBAT": np.array([4, 3.8, 4.1]),
            "I_MAC": np.array([0.1, 0.2, 0.3]),
        }
    )
    sol.plot()
