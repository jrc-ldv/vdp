import numpy as np
import schedula as sh
from vdp.physics import calc_P_el_from_V_I, calc_V_from_P_el_I


def calc_P_MAC_from_V_MAC_I_MAC(V_MAC, I_MAC):
    """The function calculates the electrical power using the voltage and current
    values of the MAC (Mobile Air Conditioning) system.

    Parameters
    ----------
    V_MAC: np.ndarray
        The voltage of the MAC (Mobile Air Conditioning) system in volts (V).
    I_MAC: np.ndarray
        The current measured by the MAC (Mobile Air Conditioning) system in amperes (A).

    Returns
    -------
    np.ndarray
        The electrical power of the MAC (Mobile Air Conditioning) system in kilowatts (kW).

    """
    return calc_P_el_from_V_I(V_MAC, I_MAC)


def calc_I_MAC_from_P_MAC_V_MAC(P_MAC, V_MAC):
    """The function calculates the current for the MAC (Mobile Air Conditioning) system using the power and voltage
    values of the MAC system.

    Parameters
    ----------
    P_MAC: np.ndarray
        The power of the MAC (Mobile Air Conditioning) system in kilowatts (kW).
    V_MAC: np.ndarray
        The voltage at the MAC (Mobile Air Conditioning) system in volts (V).

    Returns
    -------
    np.ndarray
        The current of the MAC (Mobile Air Conditioning) system in amperes (A).

    """

    return calc_V_from_P_el_I(P_MAC, V_MAC)


def calc_P_AC_COMP_from_V_MAC_I_AC_COMP(V_MAC, I_AC_COMP):
    """The function calculates the electrical power (P_AC_COMP) using the voltage (V_MAC) and current
    (I_AC_COMP) values.

    Parameters
    ----------
    V_MAC: float
        The voltage in the MAC (Mobile Air Conditioning) system in volts (V).
    I_AC_COMP: float
        The current in the AC component system in amperes (A).

    Returns
    -------
    float
        The electrical power of the AC component system in kilowatts (kW).

    """
    return calc_P_el_from_V_I(V_MAC, I_AC_COMP)


def calc_I_AC_COMP_from_P_AC_COMP_V_MAC(P_AC_COMP, V_MAC):
    """The function calculates the current in an AC component using the power and voltage of the component.

    Parameters
    ----------
    P_AC_COMP: float
        The power of the AC component in kilowatts (kW).
    V_MAC: float
        The voltage of the AC component of the power supply in volts (V).

    Returns
    -------
    float
        The current in the AC component in amperes (A).

    """
    return calc_V_from_P_el_I(P_AC_COMP, V_MAC)


def calc_P_PTC_CABIN_from_V_MAC_I_PTC_CABIN(V_MAC, I_PTC_CABIN):
    """The function calculates the power in the cabin of an aircraft based on the voltage and current
    values.

    Parameters
    ----------
    V_MAC: np.ndarray
        The voltage across the MAC (Mobile air condicioner) in volts (V).
    I_PTC_CABIN: np.ndarray
        The current flowing through the PTC cabin heater.

    Returns
    -------
    np.ndarray
        The power of the PTC cabin in kilowatts (kW).

    """

    return calc_P_el_from_V_I(V_MAC, I_PTC_CABIN)


def calc_I_PTC_CABIN_from_P_PTC_CABIN_V_MAC(P_PTC_CABIN, V_MAC):
    """The function calculates the current in the PTC cabin based on the power and voltage values.

    Parameters
    ----------
    P_PTC_CABIN: np.ndarray
        The power consumed by the cabin PTC (Positive Temperature Coefficient) heater in kilowatts (kW).
    V_MAC: np.ndarray
        The voltage across the MAC (Mobile Air Conditioning) system in volts (V).

    Returns
    -------
    np.ndarray
        The current in the PTC cabin in amperes (A).

    """

    return calc_V_from_P_el_I(P_PTC_CABIN, V_MAC)


def def_V_MAC(V_HV, V_HV_FC=0, MAC_circuit="HV"):
    """The function defines the voltage level of the MAC (Mobile Air Conditioning) system depending on which
    electric circuit it belongs to.

    Parameters
    ----------
    V_HV: np.ndarray
        The parameter V_HV represents the voltage value for the HV (High Voltage) circuit.
    V_HV_FC: np.ndarray, optional
        The voltage of the high-voltage fuel cell (HV_FC) circuit. This parameter is optional and has a
        default value of 0.
    MAC_circuit: string, optional
        String that specifies the type of circuit. It can have two possible values: "HV" or "HV_FC".
        By default, it is set to "HV".

    Returns
    -------
    np.ndarray
        The voltage of the MAC (Mobile Air Conditioning) system in volts (V).

    """

    return V_HV_FC if MAC_circuit == "HV_FC" else V_HV


def calc_I_MAC(I_AC_COMP, I_PTC_CABIN, I_WDCD=0):
    """The function calculates the current flowing through the MAC (Mobile Air Conditioning) system.

    Parameters
    ----------
    I_AC_COMP: np.ndarray
        The current flowing through the AC components of the MAC system in amperes (A).
    I_PTC_CABIN: np.ndarray
        The current flowing through the PTC cabin heater in amperes (A).
    I_WDCD: np.ndarray, optional
        I_WDCD is a parameter that represents the current flowing through the WDCD (Wide Dynamic Current
        Divider) component. It is an optional parameter with a default value of 0.

    Returns
    -------
    np.ndarray
        The current flowing through the MAC (Mobile Air Conditioning) system in amperes (A).
    """
    return I_AC_COMP + I_PTC_CABIN + I_WDCD


def calc_I_AC_COMP(I_MAC, I_PTC_CABIN, I_WDCD=0):
    """The function calculates the AC component of current by subtracting the sum of the PTC cabin current
    and the WDCD current from the MAC current.

    Parameters
    ----------
    I_MAC: np.ndarray
        The current flowing through the MAC (Mobile Air Conditioning) system in amperes (A).
    I_PTC_CABIN: np.ndarray
        The current flowing through the PTC cabin heater in amperes (A).
    I_WDCD: np.ndarray, optional
        I_WDCD is the current flowing through the WDCD (Wide Dynamic Current Divider) component. It is an
        optional parameter with a default value of 0.

    Returns
    -------
    np.ndarray
        The current flowing through the AC component in amperes (A).

    """

    return I_MAC - (I_PTC_CABIN + I_WDCD)


def calc_I_PTC_CABIN(I_MAC, I_AC_COMP, I_WDCD=0):
    """The function calculates the net current flowing through the PTC cabin by subtracting the current
    flowing through the AC components and the WDCD from the current flowing through the MAC.

    Parameters
    ----------
    I_MAC: np.ndarray
        The current flowing through the mobile air conditioning (MAC) system in amperes (A).
    I_AC_COMP: np.ndarray
        The current flowing through the AC compressor in the cabin in amperes (A).
    I_WDCD, optional
        I_WDCD stands for "Current of Windshield Defrost and Cabin Distribution" and represents the current
        flowing through the windshield defrost and cabin distribution system.

    Returns
    -------
    np.ndarray
        The current flowing through the PTC cabin in amperes (A).

    """
    return I_MAC - (I_AC_COMP + I_WDCD)


# Create a dispatcher object named "dsp"
dsp = sh.Dispatcher(name="dsp")

# Add functions to the dispatcher object with their respective outputs
dsp.add_func(calc_I_MAC_from_P_MAC_V_MAC, outputs=["I_MAC"])
dsp.add_func(calc_P_MAC_from_V_MAC_I_MAC, outputs=["P_MAC"])
dsp.add_func(calc_I_AC_COMP_from_P_AC_COMP_V_MAC, outputs=["I_AC_COMP"])
dsp.add_func(calc_P_AC_COMP_from_V_MAC_I_AC_COMP, outputs=["P_AC_COMP"])
dsp.add_func(calc_I_PTC_CABIN_from_P_PTC_CABIN_V_MAC, outputs=["I_PTC_CABIN"])
dsp.add_func(calc_P_PTC_CABIN_from_V_MAC_I_PTC_CABIN, outputs=["P_PTC_CABIN"])
dsp.add_func(def_V_MAC, outputs=["V_MAC"])
dsp.add_func(calc_I_MAC, outputs=["I_MAC"])
dsp.add_func(calc_I_AC_COMP, outputs=["I_AC_COMP"])
dsp.add_func(calc_I_PTC_CABIN, outputs=["I_PTC_CABIN"])


if __name__ == "__main__":
    dsp.plot()
    # Dispatch the inputs to the functions defined in the dispatcher object "dsp" and store the results in the "sol" variable.
    sol = dsp.dispatch(
        inputs={
            "V_HV": np.array([400, 402, 399]),
            "P_MAC": np.array([4, 3.8, 4.1]),
            "I_PTC_CABIN": np.array([1, 2.8, 2.1]),
        }
    )
    # Plot the results stored in the "sol" variable.
    sol.plot()
