"""
This module initializes the MAC (Machine Automation Controller) component.
"""

from vdp.components.mac.elec_mac import dsp as dsp_elec_mac

# Import the scheduler module
import schedula as sh

# Create a dispatcher object named "mac"
dsp = sh.Dispatcher(name="mac")

# Extend the dispatcher with the electronic MAC (dsp_elec_mac) module
dsp.extend(
    dsp_elec_mac,
    # dsp_mech_mac
)
