import numpy as np
import schedula as sh
from vdp.physics import (
    calc_P_el_from_V_I,
    calc_V_from_P_el_I,
    calc_P_me_from_T_S,
    calc_S_from_P_me_T,
)


def calc_I_MOTOR_from_I_MOTOR_front_and_rear(I_MOTOR_FRONT, I_MOTOR_REAR):
    """The function calculates the current of an equivalent motor in amperes (A) when the current from two existing
    motors (front and rear) is provided. This is a simplification to enable model compatibility with many powertrains.

    Parameters
    ----------
    I_MOTOR_FRONT: np.ndarray
        The current of the front motor (A).
    I_MOTOR_REAR: np.ndarray
        The current of the rear motor (A)

    Returns
    -------
    np.ndarray
        The current of the motor in amperes (A).

    """
    return I_MOTOR_FRONT + I_MOTOR_REAR


def calc_I_MOTOR_from_P_EL_MOTOR_V_MOTOR(P_EL_MOTOR, V_MOTOR):
    """The function calculates the motor current in amperes (A) based on its power and voltage.

    Parameters
    ----------
    P_EL_MOTOR: np.ndarray
        The motor electrical power (kW).
    V_MOTOR: np.ndarray
        The motor voltage in volts (V).

    Returns
    -------
    np.ndarray
        The current of the motor in amperes (A).

    """
    return calc_V_from_P_el_I(P_EL_MOTOR, V_MOTOR)


def calc_P_EL_MOTOR_from_I_MOTOR_V_MOTOR(I_MOTOR, V_MOTOR):
    """The function calculates the electrical power of the motor based on its current and voltage.

    Parameters
    ----------
    I_MOTOR: np.ndarray
        The motor current (A).
    V_MOTOR; np.ndarray
        The motor voltage (V).

    Returns
    -------
    np.ndarray
        The electrical power of the motor in kilowatts (kW).

    """
    return calc_P_el_from_V_I(V_MOTOR, I_MOTOR)


# TODO: Review the function to make sure that it works both for FCEVs and HEVs
def def_V_MOTOR(V_HV, V_HV_FC=0, veh_type=""):
    """The function defines the voltage level of the motor depending on the type of powertrain. V_HV is used in case of
    normal hybrid powertrains, V_HV_FC (the voltage of the fuel-cell electrical system) is used for fuel-cell vehicles.

    Parameters
    ----------
    V_HV
        V_HV is the voltage of the high-voltage system in a vehicle (V).
    V_HV_FC
        V_HV_FC is the voltage of the high-voltage fuel cell system (V).
    veh_type
        The parameter "veh_type" is a string that represents the type of vehicle.

    Returns
    -------
        The voltage level of the motor in volts (V).
    """
    return V_HV_FC if veh_type == "fcev" else V_HV


def calc_P_ME_MOTOR_from_T_MOTOR_S_MOTOR(T_MOTOR, S_MOTOR):
    """The function calculates the motor mechanical power based on the motor torque and speed.

    Parameters
    ----------
    T_MOTOR: np.ndarray
        The motor torque (Nm).
    S_MOTOR: np.ndarray
        The motor speed (rpm).

    Returns
    -------
    np.ndarray
        The mechanical power of the motor in kilowatts (kW).
    """
    return calc_P_me_from_T_S(T_MOTOR, S_MOTOR)


def calc_S_MOTOR_from_P_ME_MOTOR_T_MOTOR(P_ME_MOTOR, T_MOTOR):
    """The function calculates the rotational speed of the motor from power and torque.

    Parameters
    ----------
    P_ME_MOTOR: np.ndarray, float
        The motor mechanical power (kW).
    T_MOTOR: np.ndarray, float
        The motor torque (Nm).

    Returns
    -------
    np.ndarray, float
        The rotational speed of the motor in revolutions per minute (rpm).
    """
    return calc_S_from_P_me_T(P_ME_MOTOR, T_MOTOR)


def calc_T_MOTOR_from_P_ME_MOTOR_S_MOTOR(P_ME_MOTOR, S_MOTOR):
    """The function calculates the torque of a motor based on the power and speed of the motor.

    Parameters
    ----------
    P_ME_MOTOR
        The motor mechanical power (kW).
    S_MOTOR
        The motor speed (rpm).

    Returns
    -------
        The torque of the motor in Newton-metres (Nm).

    """
    return calc_S_from_P_me_T(P_ME_MOTOR, S_MOTOR)


def calc_P_ME_MOTOR_from_P_EL_MOTOR(P_EL_MOTOR, eff_MOTOR):
    """The function calculates the mechanical power of a motor based on the electrical power and efficiency.

    Parameters
    ----------
    P_EL_MOTOR: np.ndarray, float
        The motor electrical power (kW).
    eff_MOTOR: np.ndarray, float
        The efficiency of the motor. It is a value between 0 and 1, where 1 represents 100% efficiency.

    Returns
    -------
    np.ndarray, float
        The motor mechanical power in kilowatts (kW).
    """
    return P_EL_MOTOR * eff_MOTOR ** (np.sign(P_EL_MOTOR))


def calc_P_EL_MOTOR_from_P_ME_MOTOR(P_ME_MOTOR, eff_MOTOR):
    """The function calculates the electrical power of a motor based on the mechanical power and motor
    efficiency.

    Parameters
    ----------
    P_ME_MOTOR: np.ndarray, float
        The input power to the motor in kilo-watts (kW).
    eff_MOTOR: float
        The efficiency of the motor. It is a value between 0 and 1, where 1 represents 100% efficiency.

    Returns
    -------
    np.ndarray, float
        The electrical power of the motor in kilo-watts (kW).
    """
    return P_ME_MOTOR / eff_MOTOR ** (np.sign(P_ME_MOTOR))


def calc_motor_torque_neg(T_MOTOR):
    """The function returns the values of the motor torque if they are negative, otherwise it returns zero.

    Parameters
    ----------
    T_MOTOR: np.ndarray
        The torque of the motor (Nm).

    Returns
    -------
    np.ndarray
        The negative values of the motor torque if they are less than zero, otherwise it returns zero.
    """
    return np.where(T_MOTOR < 0, T_MOTOR, 0)


def calc_motor_power_neg(S_MOTOR, T_MOTOR_neg):
    """The function calculates the negative motor power based on the motor speed and negative torque values.

    Parameters
    ----------
    S_MOTOR: np.ndarray
        The motor speed (rpm).
    T_MOTOR_neg: np.ndarray
        The motor negative torque (Nm).

    Returns
    -------
    np.ndarray
        The negative motor power in kilowatts (kW).

    """
    with np.errstate(invalid="ignore"):
        return S_MOTOR * T_MOTOR_neg * 2 * np.pi / 60 / 1000


def calc_P_ME_MG1(T_MG1, S_MG1):
    """The function calculates the mechanical power of the Motor Generator 1 (machine connected to the sun gear of the
     planetary system) in power-split hybrids.

    Parameters
    ----------
    T_MG1: np.ndarray
        The torque of the MG1 (Motor Generator 1) (Nm).
    S_MG1: np.ndarray
        The speed of the MG1 (Motor Generator 1) (rpm).

    Returns
    -------
    np.ndarray
        The mechanical power of the MG1 (Motor Generator 1) in kilowatts (kW).

    """
    return calc_P_ME_MOTOR_from_T_MOTOR_S_MOTOR(T_MG1, S_MG1)


def calc_P_ME_MG2(T_MG2, S_MG2):
    """The function calculates the mechanical power of the Motor Generator 2 (machine connected to the ring gear of the
     planetary system) in power-split hybrids.

    Parameters
    ----------
    T_MG2: np.ndarray
        The torque of the MG2 (Motor Generator 2) (Nm).
    S_MG2: np.ndarray
        The speed of the MG2 (Motor Generator 2) (rpm).

    Returns
    -------
    np.ndarray
        The mechanical power of the MG2 (Motor Generator 2) in kilowatts (kW).

    """
    return calc_P_ME_MOTOR_from_T_MOTOR_S_MOTOR(T_MG2, S_MG2)


def calc_P_ME_MOTOR_neg_from_P_ME_MG1_P_ME_MG2(P_ME_MG1, P_ME_MG2, ice_power_out):
    """The function calculates the negative mechanical power of the planetary system seen as an equivalent motor,
    based on the power of Motor Generator 1 and 2 and the power of the internal combustion engine.

    Parameters
    ----------
    P_ME_MG1: np.ndarray
        The power of motor-generator 1 (MG1) (kW).
    P_ME_MG2: np.ndarray
        The power of motor-generator 2 (MG2) (kW).
    ice_power_out: np.ndarray
        The power of the internal combustion engine (kW).

    Returns
    -------
    np.ndarray
        The negative mechanical power of planetary system seen as an equivalent motor in kilowatts (kW).

    """

    P_ME_MOTOR_neg = np.where(ice_power_out >= 0, P_ME_MG2, P_ME_MG2 + P_ME_MG1)
    P_ME_MOTOR_neg = np.where(P_ME_MOTOR_neg >= 0, 0, P_ME_MOTOR_neg)

    return P_ME_MOTOR_neg


# The code block you provided is creating a `Dispatcher` object from the `schedula` library and adding
# several functions to it.
dsp = sh.Dispatcher(name="dsp")
dsp.add_func(calc_I_MOTOR_from_I_MOTOR_front_and_rear, outputs=["I_MOTOR"])
dsp.add_func(calc_I_MOTOR_from_P_EL_MOTOR_V_MOTOR, outputs=["I_MOTOR"])
dsp.add_func(calc_P_EL_MOTOR_from_I_MOTOR_V_MOTOR, outputs=["P_EL_MOTOR"])
dsp.add_func(def_V_MOTOR, outputs=["V_MOTOR"])
dsp.add_func(calc_P_ME_MOTOR_from_T_MOTOR_S_MOTOR, outputs=["P_ME_MOTOR"])
dsp.add_func(calc_S_MOTOR_from_P_ME_MOTOR_T_MOTOR, outputs=["S_MOTOR"])
dsp.add_func(calc_T_MOTOR_from_P_ME_MOTOR_S_MOTOR, outputs=["T_MOTOR"])
dsp.add_func(calc_P_ME_MOTOR_from_P_EL_MOTOR, outputs=["P_ME_MOTOR"])
dsp.add_func(calc_P_EL_MOTOR_from_P_ME_MOTOR, outputs=["P_EL_MOTOR"])
dsp.add_func(calc_motor_torque_neg, outputs=["T_MOTOR_neg"])
dsp.add_func(calc_motor_power_neg, outputs=["P_ME_MOTOR_neg"])
dsp.add_func(calc_P_ME_MG1, outputs=["P_ME_MG1"])
dsp.add_func(calc_P_ME_MG2, outputs=["P_ME_MG2"])
dsp.add_func(calc_P_ME_MOTOR_neg_from_P_ME_MG1_P_ME_MG2, outputs=["ems_power_neg"])


if __name__ == "__main__":
    # dsp.plot()
    # sol = dsp.dispatch(inputs={'V_HV': np.array([400, 402, 399]),
    #                            'I_MOTOR': np.array([41, 42.8, 42.1]),
    #                            'eff_MOTOR': 0.9,
    #                            'S_MOTOR':[2000, 1900, 2100]})
    # The code `dsp.plot()` is used to visualize the computation graph created by the `Dispatcher`
    # object `dsp`. It shows the flow of data between the functions and their inputs and outputs.
    dsp.plot()
    # The code `sol = dsp.dispatch(inputs={"P_EL_MOTOR": np.array([41, 42.8, 42.1, -10, -20]),
    # "eff_MOTOR": 0.9})` is using the `dispatch` method of the `Dispatcher` object `dsp` to compute
    # the outputs of the functions in the computation graph.
    sol = dsp.dispatch(
        inputs={"P_EL_MOTOR": np.array([41, 42.8, 42.1, -10, -20]), "eff_MOTOR": 0.9}
    )
    # The `sol.plot()` function is used to visualize the results of the computation graph. It displays
    # the inputs and outputs of each function in the graph, allowing you to see how the data flows
    # through the graph and how the outputs are calculated based on the inputs. This can be helpful
    # for understanding the relationships between the different functions and verifying the
    # correctness of the calculations.
    sol.plot()
