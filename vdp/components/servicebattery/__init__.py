import schedula as sh
from vdp.physics import calc_P_el_from_V_I


def calc_P_LVBAT_from_V_LV_I_LVBAT(V_LVBAT, I_LVBAT):
    """The function calculates the low-voltage battery electrical power from its voltage and current.

    Parameters
    ----------
    V_LVBAT: np.ndarray
        The voltage of the low-voltage battery in volts (V).
    I_LVBAT: np.ndarray
        The current of the low-voltage battery in amperes (A).

    Returns
    -------
    np.ndarray
        The low-voltage battery power in kilo-watts (kW).

    """
    return calc_P_el_from_V_I(V_LVBAT, I_LVBAT)


# The code block you provided is creating a `Dispatcher` object named `dsp` and adding several
# functions to it.
dsp = sh.Dispatcher(name="dsp")
dsp.add_func(calc_P_LVBAT_from_V_LV_I_LVBAT, outputs=["P_LVBAT"])


if __name__ == "__main__":
    # The `dsp.plot()` function is used to visualise the computation graph of the `Dispatcher` object
    # `dsp`. It shows the flow of data between the input and output variables, as well as the
    # functions that are applied to the data. This can help in understanding the structure and
    # dependencies of the computation.
    dsp.plot()
