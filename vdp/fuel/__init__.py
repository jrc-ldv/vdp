import numpy as np
import schedula as sh
from scipy.integrate import cumulative_trapezoid


# Dictionary mapping fuel types to their lower heating values (LHV) in J/g
lhv_dict = {"gasoline": 43200, "diesel": 43100, "hydrogen": 119960}

# Dictionary mapping fuel types to their carbon content in mass fraction
fuel_c_dict = {"gasoline": 86.3, "diesel": 86.3, "hydrogen": 0}

# Dictionary mapping fuel types to their densities in grams/litre
fuel_density_dict = {
    "gasoline": 745.0,
    "diesel": 832.0,
    "hydrogen": 0.08988,  # grams/litre # hydrogen density at ambient pressure?
}

# Dictionary mapping fuel types to their CO2 emission factors in kg/kg
co2_fuel_ratio_dict = {"gasoline": 3.17, "diesel": 3.16, "hydrogen": 0}


def get_fuel_density(fuel_type):
    """
    Get the density of a specific fuel type.

    Parameters
    ----------
    fuel_type: str
        The type of fuel.

    Returns
    -------
    float
        The density of the fuel in kg/m^3.

    .. note::
        The density of gasoline is 750 kg/m^3, diesel is 832 kg/m^3, and
        hydrogen is 0.08988 kg/m^3.

    Example
    -------
    >>> get_fuel_density('gasoline')
    750.0
    """
    return fuel_density_dict[fuel_type]


# TODO: update documentation
def calc_fuel_cumulative_kg(TIME, fuel_rate_g_s):
    return cumulative_trapezoid(fuel_rate_g_s, TIME, initial=0) / 1000


# TODO: update documentation
def calc_fuel_rate_g_s_from_l_h(fuel_rate_l_h, fuel_density):
    # fuel_density in grams/litre
    return fuel_rate_l_h / 3600 * fuel_density


# TODO: update documentation
def calc_delta_FUEL_g(fuel_cumulative_kg):
    return np.diff(fuel_cumulative_kg, prepend=0) * 1000


# Create a dispatcher object named "dsp"
dsp = sh.Dispatcher(name="dsp")

# Add the function get_fuel_density to the dispatcher, with the output variable "fuel_density"
dsp.add_func(get_fuel_density, outputs=["fuel_density"])

# TODO: comment needed
dsp.add_func(calc_fuel_cumulative_kg, outputs=["fuel_cumulative_kg"])

# Add the function calc_fuel_rate_g_s_from_l_h to the dispatcher, with the output variable "fuel_rate_g_s"
dsp.add_func(calc_fuel_rate_g_s_from_l_h, outputs=["fuel_rate_g_s"])

# TODO: comment needed
dsp.add_func(calc_delta_FUEL_g, outputs=["delta_FUEL_g"])


if __name__ == "__main__":
    dsp.plot()
