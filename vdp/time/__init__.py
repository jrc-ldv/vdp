import schedula as sh
import numpy as np


def pandas_timestamp_to_time_sds(PANDAS_TIMESTAMP):
    """
    Converts descriptive time taken as float (hh:mm:ss in float 0->1) to absolute time since day start.

    Parameters
    ----------
    PANDAS_TIMESTAMP : pd.Series
        Time represented as pandas Series containing timestamps.

    Returns
    -------
    float
        Time since day start in seconds.

    .. note::
    This function assumes that the input pandas Series contains timestamps in the format hh:mm:ss, represented as floats ranging from 0 to 1.

    .. example::
    >>> import pandas as pd
    >>> timestamps = pd.Series([0.5, 0.75, 0.25])
    >>> pandas_timestamp_to_time_sds(timestamps)
    43200.0, 64800.0, 21600.0
    """

    return PANDAS_TIMESTAMP.apply(
        lambda x: x.hour * 3600 + x.minute * 60 + x.second + x.microsecond / 1000
    )


def desc_time_to_time_sds(DESC_TIME):
    """
    Converts descriptive time taken as float (hh:mm:ss in float 0->1) to absolute time since day start.

    Parameters
    ----------
    DESC_TIME: float
        Time in float format representing hours, minutes, and seconds.

    Returns
    -------
    float
        Time in seconds since the start of the day.

    .. note::
    The DESC_TIME parameter should be a float value between 0 and 1, representing the fraction of a day.
    For example, 0.5 represents 12:00:00 PM.

    .. example::
    >>> desc_time_to_time_sds(0.5)
    43200.0
    >>> desc_time_to_time_sds(0.25)
    21600.0
    """
    return DESC_TIME * 24 * 60 * 60


def time_sds_to_desc_time(TIME_SDS):
    """
    Converts absolute time since day start to descriptive time taken as float (hh:mm:ss in float 0->1).

    Parameters
    ----------
    TIME_SDS : float
        Time since day start in seconds.

    Returns
    -------
    float
        Descriptive time as a float between 0 and 1.

    .. note::
    The function converts the absolute time since day start to a descriptive time in the format hh:mm:ss
    represented as a float between 0 and 1. The input time should be in seconds.

    .. example::
    >>> time_sds_to_desc_time(3600)
    0.041666666666666664
    >>> time_sds_to_desc_time(7200)
    0.08333333333333333
    """

    return TIME_SDS / 24 / 60 / 60


def time_sds_to_rel_time(TIME_SDS, offset=0):
    """
    Converts absolute time since day start to relative time since event start.

    Parameters
    ----------
    TIME_SDS : float
        Time since day start in seconds.

    offset : float, optional
        Offset to be added to the relative time. Default is 0.

    Returns
    -------
    float
        Relative time since event start.

    .. note::
    This function calculates the relative time since the start of an event
    based on the absolute time since the start of the day. The offset parameter
    allows for additional time to be added to the relative time.

    .. example::
    >>> time_sds_to_rel_time(3600)
    0.0

    >>> time_sds_to_rel_time(7200, offset=10)
    10.0
    """
    return TIME_SDS - TIME_SDS[0] + offset


def calc_delta_TIME(TIME):
    """
    Converts absolute time since day start to relative time since event start.

    Parameters
    ----------
    TIME : array_like
        Array of time values representing absolute time since day start.

    Returns
    -------
    ndarray
        Array of delta time values representing relative time since event start.

    .. note::
    This function calculates the difference between consecutive time values in the input array.
    The first element of the output array is always 0, as there is no previous time value to compare with.

    .. example::
    >>> TIME = np.array([0, 1, 3, 6, 10])
    >>> calc_delta_TIME(TIME)
    array([0, 1, 2, 3, 4])

    >>> TIME = np.array([0, 2, 5, 9, 15])
    >>> calc_delta_TIME(TIME)
    array([0, 2, 3, 4, 6])
    """
    return np.append(0, np.diff(TIME))


# Create a dispatcher object named "dsp"
dsp = sh.Dispatcher(name="dsp")

# Add functions to the dispatcher object with their respective outputs
dsp.add_func(desc_time_to_time_sds, outputs=["TIME_SDS"])
dsp.add_func(time_sds_to_desc_time, outputs=["DESC_TIME"])
dsp.add_func(time_sds_to_rel_time, outputs=["TIME"])
dsp.add_func(pandas_timestamp_to_time_sds, outputs=["TIME_SDS"])
dsp.add_func(calc_delta_TIME, outputs=["delta_TIME"])


if __name__ == "__main__":

    # Plot the initial state of the dispatcher object
    dsp.plot()

    # Dispatch the inputs to the functions in the dispatcher object
    sol = dsp.dispatch(inputs={"TIME_SDS": np.array([1000, 1001, 1002])})

    # Plot the resulting state after dispatching the inputs
    sol.plot()
