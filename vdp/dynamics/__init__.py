import numpy as np
import pandas as pd
from scipy.interpolate import interp1d
from scipy.integrate import cumulative_trapezoid
from scipy.signal import butter, filtfilt
from scipy import signal
import schedula as sh
from vdp.algorithms import calculate_smooth_speed_from_trusted_values


def def_OBD_SPEED_CORRECTION_FACTOR_default():
    """
    Returns the default value for the OBD speed correction factor.

    Returns
    -------
    int
        The default value for the OBD speed correction factor, which is 1.

    .. note::
    The OBD speed correction factor is used to adjust the speed readings obtained from the OBD system.
    It is typically set to 1, indicating no correction is applied.

    .. example::
    >>> def_OBD_SPEED_CORRECTION_FACTOR_default()
    1
    """
    return 1


def def_OBD_SPEED_CORRECTION_FACTOR_from_GPS(OBD_SPEED, GPS_SPEED, GPS_POS_VALID):
    """
    Calculate the OBD speed correction factor based on GPS speed and position validity.

    Parameters
    ----------
    OBD_SPEED : np.ndarray
        The speed values obtained from the OBD (On-Board Diagnostics) system.
    GPS_SPEED : np.ndarray
        The GPS speed values.
    GPS_POS_VALID : np.ndarray
        Boolean array indicating whether the GPS position is valid or not for each data point.

    Returns
    -------
    float
        The OBD speed correction factor calculated based on the OBD speed and GPS speed. If the absolute
        value of the OBD speed median is greater than 10, it returns the ratio of the GPS speed median to
        the OBD speed median. Otherwise, it returns 1.

    .. notes::
    - The function assumes that OBD_SPEED, GPS_SPEED, and GPS_POS_VALID have the same length.
    - The function ignores data points where GPS position is not valid.
    - The function calculates the median of OBD_SPEED and GPS_SPEED after filtering out invalid data points.

    .. example::
    >>> OBD_SPEED = np.array([10, 20, 30, 40, 50])
    >>> GPS_SPEED = np.array([15, 25, 35, 45, 55])
    >>> GPS_POS_VALID = np.array([True, True, False, True, True])
    >>> def_OBD_SPEED_CORRECTION_FACTOR_from_GPS(OBD_SPEED, GPS_SPEED, GPS_POS_VALID)
    1.25
    """
    GPS_SPEED_OK = np.array(GPS_SPEED)[np.where(GPS_POS_VALID)[0]]
    OBD_SPEED_OK = np.array(OBD_SPEED)[np.where(GPS_POS_VALID)[0]]
    GPS_SPEED_median = np.nanmedian(GPS_SPEED_OK)
    OBD_SPEED_median = np.nanmedian(OBD_SPEED_OK)
    if abs(OBD_SPEED_median) > 10:
        return GPS_SPEED_median / OBD_SPEED_median
    else:
        return 1.0


# TODO: Add funciton with the speed correction based on f0, f1, f2, test_mass, and inertia_factor


def def_OBD_SPEED_CORRECTION_FACTOR_from_DYNO(OBD_SPEED, DYNO_SPEED):
    """
    Calculate the OBD speed correction factor based on the median values of OBD speed and dyno speed.

    Parameters
    ----------
    OBD_SPEED: np.ndarray
        The speed readings obtained from the On-Board Diagnostics (OBD) system of a vehicle. These readings
        are typically obtained from sensors that measure the vehicle's speed.
    DYNO_SPEED: np.ndarray
        The speed values measured by a dynamometer.

    Returns
    -------
    float
        The OBD speed correction factor calculated based on the OBD speed and dyno speed inputs. If the
        absolute value of the OBD speed median is greater than 10, it returns the ratio of the dyno speed
        median to the OBD speed median. Otherwise, it returns 1.

    .. notes::
    The OBD speed correction factor is calculated as follows:
    - If the absolute value of the OBD speed median is greater than 10, the OBD speed correction factor is
    calculated as the ratio of the dyno speed median to the OBD speed median.
    - If the absolute value of the OBD speed median is less than or equal to 10, the OBD speed correction
    factor is set to 1.

    Examples
    --------
    >>> OBD_SPEED = [5, 10, 15, 20, 25]
    >>> DYNO_SPEED = [10, 20, 30, 40, 50]
    >>> def_OBD_SPEED_CORRECTION_FACTOR_from_DYNO(OBD_SPEED, DYNO_SPEED)
    2.0

    >>> OBD_SPEED = [5, 10, 15, 20, 25]
    >>> DYNO_SPEED = [50, 40, 30, 20, 10]
    >>> def_OBD_SPEED_CORRECTION_FACTOR_from_DYNO(OBD_SPEED, DYNO_SPEED)
    0.5
    """
    DYNO_SPEED_median = np.nanmedian(DYNO_SPEED)
    OBD_SPEED_median = np.nanmedian(OBD_SPEED)
    if abs(OBD_SPEED_median) > 10:
        return DYNO_SPEED_median / OBD_SPEED_median
    else:
        return 1.0


def process_OBD_SPEED(TIME, OBD_SPEED, OBD_SPEED_CORRECTION_FACTOR):
    """
    The function calibrates the OBD speed with a correction factor to calculate the final speed.

    Parameters
    ----------
    OBD_SPEED: np.ndarray
        The speed value obtained from the OBD (On-Board Diagnostics) system of a vehicle.
    OBD_SPEED_CORRECTION_FACTOR  float
        Value used to calibrate the OBD_SPEED with the GPS_SPEED. It is a factor that is
        multiplied with the OBD_SPEED to obtain the calibrated SPEED value.

    Returns
    -------
    np.ndarray
        The calibrated speed value.

    .. notes::
    The calibration of the OBD_SPEED with GPS_SPEED is performed by multiplying the OBD_SPEED
    with the OBD_SPEED_CORRECTION_FACTOR.

    .. example::
    >>> OBD_SPEED = np.array([50, 60, 70])
    >>> OBD_SPEED_CORRECTION_FACTOR = 0.9
    >>> def_SPEED(OBD_SPEED, OBD_SPEED_CORRECTION_FACTOR)
    array([45., 54., 63.])
    """

    # calibration of OBD_SPEED with GPS_SPEED

    us_res = 0.1  # time step to be used for the upsampling
    time_us = np.arange(min(TIME), max(TIME) + us_res, us_res)

    speed_us, trusted_time, trusted_speed = calculate_smooth_speed_from_trusted_values(
        TIME, OBD_SPEED, time_us
    )
    acceleration_us = np.gradient(speed_us / 3.6, time_us)

    SPEED_RAW = interp1d(time_us, speed_us)(TIME)
    ACCELERATION = interp1d(time_us, acceleration_us)(TIME)

    SPEED = SPEED_RAW * OBD_SPEED_CORRECTION_FACTOR
    return SPEED, ACCELERATION


def calc_DISTANCE(TIME, SPEED):
    """
    Calculate the distance traveled based on time and speed data.

    Parameters
    ----------
    TIME : np.ndarray
        The time intervals at which the speed is measured in seconds (s).
    SPEED : np.ndarray
        The speed of an object at different points in time is measured in kilometers per hour (km/h).

    Returns
    -------
    np.ndarray
        The distance traveled at different points in time (in kilometers).

    .. note::
    The function uses numerical integration to calculate the distance traveled.
    The integration is performed using the trapezoidal rule.

    Example
    -------
    >>> time = np.array([0, 1, 2, 3, 4])
    >>> speed = np.array([0, 10, 20, 30, 40])
    >>> calc_DISTANCE(time, speed)
    array([ 0.        ,  0.00555556,  0.02222222,  0.05      ,  0.08888889])
    """
    return cumulative_trapezoid(SPEED, TIME, initial=0) / 3.6 / 1000


def calc_ACCELERATION(SPEED, TIME):
    """
    Calculate the acceleration based on speed and time measurements,
    applying filtering and thresholding operations.

    Parameters
    ----------
    SPEED: np.ndarray
        The speed of an object at different points in time is measured in kilometers per hour (km/h).
    TIME: np.ndarray
        The time intervals at which the speed is measured in seconds (s).

    Returns
    -------
    np.ndarray
        The acceleration of the object at different points in time in meters per second squared (m/s^2).

    Notes
    -----
    The function calculates the acceleration by taking the derivative of the speed with respect to time.
    It applies filtering to the raw acceleration values and sets acceleration to 0 when speed is below 0.1 km/h.

    Example
    -------
    >>> SPEED = np.array([10, 20, 30])  # km/h
    >>> TIME = np.array([1, 2, 3])  # s
    >>> calc_ACCELERATION(SPEED, TIME)
    array([0.27777778, 0.27777778, 0.27777778])
    """

    # raw_acceleration = np.gradient(SPEED / 3.6, TIME)
    #
    # if raw_acceleration.any():
    #     diffs = np.diff(TIME)
    #     std = np.std(diffs)
    #     median = np.median(diffs)
    #     filtered_diffs = diffs[
    #         (diffs >= median - 3 * std) & (diffs <= median + 3 * std)
    #     ]
    #
    #     sf = np.round(
    #         1 / np.mean(filtered_diffs), 0
    #     )  # calculates sampling frequency from experimental data
    #
    #     cut_off = 0.2  # cut_off frequency
    #     nyq = 0.5 * sf
    #     n = 1  # Filter order
    #     fc = cut_off / nyq  # Cutoff frequency normal
    #     b, a = signal.butter(n, fc)
    #
    #     ACCELERATION = signal.filtfilt(b, a, np.nan_to_num(raw_acceleration))
    # else:
    #     ACCELERATION = raw_acceleration
    #
    # ACCELERATION = np.where(SPEED > 0.1, ACCELERATION, 0)

    ACCELERATION = np.diff(SPEED) / 3.6 / np.diff(TIME)
    ACCELERATION = np.append(ACCELERATION[0], ACCELERATION)

    return ACCELERATION


def def_inertia_factor(veh_type):
    """
    Return the inertia factor based on the vehicle type provided as an argument.

    Parameters
    ----------
    veh_type : str
        The type of vehicle. It can have one of the following values:
        "mhev", "hev", "phev", "ev", "ice", or "fcev".

    Returns
    -------
    float
        The inertia factor for the vehicle type provided as an argument.

    .. note::
    The inertia factor represents the ratio of the actual inertia of the vehicle
    to the reference inertia. It is used in vehicle dynamics calculations.

    .. example::
    >>> def_inertia_factor("hev")
    1.037
    >>> def_inertia_factor("ev")
    1.03
    """

    inertia_factor = {
        "mhev": 1.033,
        "hev": 1.037,
        "phev": 1.04,
        "ev": 1.03,
        "ice": 1.03,
        "fcev": 1.03,
    }
    return inertia_factor[veh_type]


def calc_mass_ratio_coc(test_mass, test_mass_coc):
    """

    Parameters
    ----------
    test_mass: float
        The actual test mass of the vehicle in kg.
    test_mass_coc: float
        The test mass from the certificate of conformity of the vehicle in kg.

    Returns
    -------

    """
    return test_mass / test_mass_coc


def calc_f2_temp_ratio(T_AMB):
    """

    Parameters
    ----------
    T_AMB: np.ndarray
        The array of ambient temperature measured in the test in degC.
    Returns
    -------

    """
    return 293.15 / np.median(T_AMB + 273.15)


def calc_MOTIVE_POWER(
    SPEED,
    ACCELERATION,
    ANGLE_SLOPE,
    f0,
    f1,
    f2,
    test_mass,
    inertia_factor,
    f2_temp_ratio,
    mass_ratio_coc,
):
    """
    Calculate the motive power of a vehicle based on speed, acceleration, slope angle, and vehicle parameters.

    Parameters
    ----------
    SPEED: np.ndarray
        The speed of the vehicle in kilometers per hour (km/h).
    ACCELERATION: np.ndarray
        The acceleration of the vehicle in meters per second squared (m/s^2).
    ANGLE_SLOPE: np.ndarray
        The angle of the slope on which the vehicle is traveling. It is used in the calculation of the
        motive power of the vehicle.
    f0: float
        The rolling resistance and other factors that affect the motive power of the vehicle when it
        is not accelerating or decelerating.
    f1: float
        The coefficient of the linear term in the equation for motive power. It is a constant that determines
        the contribution of speed to the motive power calculation.
    f2: float
        The coefficient of the quadratic term in the equation for motive power calculation. It is used to
        model the effect of speed on the motive power of the vehicle.
    test_mass: float
        The test mass of the vehicle.
    inertia_factor: float
        The inertia factor is a parameter that represents the effect of inertia on the motive power of a
        vehicle. It is multiplied by the vehicle test mass and the acceleration to calculate the
        contribution of inertia to the motive power.
    f2_temp_ratio: float
        The ratio between the median test temperature and the road loads reference temperature (20°C),
        used for road loads normalisation.
    mass_ratio_coc: float
        The ratio between the actual test mass and the test mass in the certificate of conformity of the
        vehicle, used for road loads normalisation.

    Returns
    -------
    tuple
        The motive power at the wheels of the vehicle in kilowatts (kW) and the normalised road loads f0_norm, f1_norm
        and f2_norm.

    Notes
    -----
    The motive power is calculated using the following equation:

    .. math::
        \\left(f0 + f1 \\cdot SPEED + f2 \\cdot SPEED^2 + inertia_factor \\cdot test_mass \\cdot ACCELERATION +
        \\sin(ANGLE_SLOPE) \\cdot test_mass \\cdot 9.81\\right) \\cdot SPEED / 3.6 / 1000

    - The motive power is influenced by factors such as speed, acceleration, slope angle, and vehicle parameters.
    - The motive power is expressed in kilowatts (kW).
    - The input arrays SPEED, ACCELERATION, and ANGLE_SLOPE should have the same shape.

    Example
    -------
    >>> SPEED = np.array([60, 80, 100])
    >>> ACCELERATION = np.array([2, 1, 0])
    >>> ANGLE_SLOPE = np.array([0, 0, 0])
    >>> f0 = 0.1
    >>> f1 = 0.01
    >>> f2 = 0.001
    >>> test_mass = 1500
    >>> inertia_factor = 0.05
    >>> calc_MOTIVE_POWER(SPEED, ACCELERATION, ANGLE_SLOPE, f0, f1, f2, test_mass, inertia_factor)
    array([ 4.16666667,  3.33333333,  2.5       ])
    """

    f0_norm = f0 * mass_ratio_coc
    f1_norm = f1
    f2_norm = f2 * f2_temp_ratio

    SPEED_mean_ts = SPEED - np.append(
        0, np.diff(SPEED) / 2
    )  # calculation of mean speed within the timestep

    motive_powers = (
        (
            f0_norm * np.cos(ANGLE_SLOPE)
            + f1_norm * SPEED_mean_ts
            + f2_norm * SPEED_mean_ts ** 2
            + inertia_factor * test_mass * ACCELERATION
            + np.sin(ANGLE_SLOPE) * test_mass * 9.81
        )
        * SPEED_mean_ts
        / 3.6
        / 1000
    )

    return motive_powers, f0_norm, f1_norm, f2_norm


def clean_ALTITUDE_RAW(TIME, ALTITUDE_RAW):
    """
    Clean the raw altitude data by applying a rolling mean filter to the raw altitude data.

    Parameters
    ----------
    TIME : np.ndarray
        The time values for each altitude measurement.
    ALTITUDE_RAW : np.ndarray
        The raw altitude data.

    Returns
    -------
    np.ndarray
        The cleaned altitude data.

    .. note::
    The function applies a rolling mean filter to the raw altitude data to smooth out noise and fluctuations.
    It calculates the sampling frequency based on the time values and applies a rolling mean filter with a
    window size of 100 times the sampling frequency.
    The function also handles edge cases by replicating the first and last values of the filtered data.

    .. example::
    >>> TIME = np.array([0, 1, 2, 3, 4, 5])
    >>> ALTITUDE_RAW = np.array([10, 12, 15, 13, 11, 9])
    >>> clean_ALTITUDE_RAW(TIME, ALTITUDE_RAW)
    array([10.        , 11.        , 12.        , 13.        , 11.        ,  9.        ])
    """

    rolling_window_multiplier = 50

    if (len(TIME) > rolling_window_multiplier + 2) and np.sum(ALTITUDE_RAW) != 0:
        sf = max(int(np.round(1 / np.mean(np.diff(TIME)), 0)), 1)
        interval = sf * rolling_window_multiplier
        index = int(interval / 2)
        altitude_clean = (
            pd.Series(ALTITUDE_RAW).rolling(interval, center=True).mean().values
        )
        altitude_clean[: index + 1] = altitude_clean[index + 2]
        altitude_clean[-index - 1 :] = altitude_clean[-index - 2]
    elif np.sum(ALTITUDE_RAW) != 0:
        altitude_clean = ALTITUDE_RAW
    else:
        altitude_clean = np.zeros_like(TIME)
    return altitude_clean


def process_ALTITUDE(TIME, SPEED, ALTITUDE):
    """The function calculates the angle slopes based on time, speed, and altitude data.

    Parameters
    ----------
    TIME : np.ndarray
        The time values in seconds (s) at which the speed and altitude are measured.
    SPEED : np.ndarray
        The vehicle speed in kilometers per hour (km/h).
    ALTITUDE : np.ndarray
        The altitude values at different time points.

    Returns
    -------
    tuple
        The angle slopes arrays in radians and percent points.

    Notes
    -----
    This function calculates the angle slopes by integrating the speed and altitude data.
    It uses the cumulative trapezoidal integration method to calculate the distances traveled.
    The angle slopes are then calculated as the arcsine of the ratio of altitude change to distance change.
    The resulting angle slopes are interpolated to match the distances traveled.

    Example
    -------
    >>> TIME = np.array([0, 1, 2, 3, 4])
    >>> SPEED = np.array([60, 70, 80, 90, 100])
    >>> ALTITUDE = np.array([0, 10, 20, 30, 40])
    >>> calc_ANGLE_SLOPE(TIME, SPEED, ALTITUDE)
    array([0.        , 0.17453293, 0.17453293, 0.17453293, 0.17453293])
    """
    if any(ALTITUDE) & (np.median(ALTITUDE) != -999):
        min_speed = 20  # min speed for slope impact calculation (for stability)

        distances = cumulative_trapezoid(SPEED / 3.6, TIME, initial=0)
        d = np.diff(distances, prepend=0)
        d_fil = np.where(SPEED < min_speed, 0.1, d)
        dh = np.diff(ALTITUDE, prepend=0)
        dh_fil = np.where(SPEED < min_speed, 0, dh)
        angle_slopes_raw = np.arcsin(dh_fil / d_fil)
        df_slopes = pd.DataFrame(
            data=dict(distances=distances, angle_slopes_raw=angle_slopes_raw)
        )
        df_slopes = df_slopes[df_slopes["angle_slopes_raw"] != 0].reset_index(drop=True)
        df_slopes = pd.concat(
            [df_slopes, pd.DataFrame({"distances": [0], "angle_slopes_raw": [0]})],
            ignore_index=True,
        )
        df_slopes = pd.concat(
            [
                df_slopes,
                pd.DataFrame(
                    data={"distances": [max(distances)], "angle_slopes_raw": [0]}
                ),
            ],
            ignore_index=True,
        ).sort_values(by="distances")

        # Ensure that there are enough points for cubic interpolation
        if len(df_slopes) > 3:
            angle_slopes = interp1d(
                df_slopes["distances"].values,
                df_slopes["angle_slopes_raw"],
                kind="cubic",
                bounds_error=False,
                fill_value=(0, 0),
            )(distances)
        else:
            angle_slopes = interp1d(
                df_slopes["distances"].values,
                df_slopes["angle_slopes_raw"],
                kind="linear",
                bounds_error=False,
                fill_value=(0, 0),
            )(distances)
        percent_slopes = 100 * np.tan(angle_slopes)

        return angle_slopes, percent_slopes
    else:
        return np.zeros_like(TIME), np.zeros_like(TIME)


def calc_delta_DISTANCE(DISTANCE):
    """
    Calculate the difference between consecutive distances.

    Parameters
    ----------
    DISTANCE : np.ndarray
        An array representing a series of distances in kilometers (km).

    Returns
    -------
    np.ndarray
        An array representing the difference between consecutive distances.

    .. note::
    This function calculates the difference between consecutive distances by
    subtracting each distance from its previous distance. The first element
    of the returned array is always 0, as there is no previous distance for
    the first element.

    .. example::
    >>> distances = np.array([10, 15, 20, 25])
    >>> calc_delta_DISTANCE(distances)
    array([0, 5, 5, 5])
    """

    return np.append(0, np.diff(DISTANCE))


def def_SPEED_from_DYNO_SPEED(DYNO_SPEED):
    """
    Convert the dynamometer speed to the speed value.

    Parameters
    ----------
    DYNO_SPEED : np.ndarray
        The speed measured by a dynamometer.

    Returns
    -------
    np.ndarray
        The speed measured by the dynamometer.

    .. note::
    This function converts the speed measured by a dynamometer to the speed value.

    .. example::
    >>> dyno_speed = np.array([10, 20, 30])
    >>> def_SPEED_from_DYNO_SPEED(dyno_speed)
    array([10, 20, 30])
    """
    return DYNO_SPEED


# Create a dispatcher object named "dsp"
dsp = sh.Dispatcher(name="dsp")

# Add the function def_OBD_SPEED_CORRECTION_FACTOR_from_DYNO to the dispatcher,
# with the output "OBD_SPEED_CORRECTION_FACTOR"
dsp.add_func(
    def_OBD_SPEED_CORRECTION_FACTOR_from_DYNO, outputs=["OBD_SPEED_CORRECTION_FACTOR"]
)

# Add the function def_OBD_SPEED_CORRECTION_FACTOR_from_GPS to the dispatcher,
# with the output "OBD_SPEED_CORRECTION_FACTOR" and a weight of 1
dsp.add_func(
    def_OBD_SPEED_CORRECTION_FACTOR_from_GPS,
    outputs=["OBD_SPEED_CORRECTION_FACTOR"],
    weight=1,
)

# Add the function def_OBD_SPEED_CORRECTION_FACTOR_default to the dispatcher,
# with the output "OBD_SPEED_CORRECTION_FACTOR" and a weight of 2
dsp.add_func(
    def_OBD_SPEED_CORRECTION_FACTOR_default,
    outputs=["OBD_SPEED_CORRECTION_FACTOR"],
    weight=2,
)

# Add the function def_SPEED to the dispatcher, with the output "SPEED" and a weight of 1
dsp.add_func(process_OBD_SPEED, outputs=["SPEED", "ACCELERATION"], weight=2)

# Add the function calc_DISTANCE to the dispatcher, with the output "DISTANCE"
dsp.add_func(calc_DISTANCE, outputs=["DISTANCE"])

# Add the function calc_delta_DISTANCE to the dispatcher, with the output "delta_DISTANCE"
dsp.add_func(calc_delta_DISTANCE, outputs=["delta_DISTANCE"])

# Add the function calc_ACCELERATION to the dispatcher, with the output "ACCELERATION"
dsp.add_func(calc_ACCELERATION, outputs=["ACCELERATION"])

# Add the function def_inertia_factor to the dispatcher, with the output "inertia_factor"
dsp.add_func(def_inertia_factor, outputs=["inertia_factor"])

# Add the function calc_MOTIVE_POWER to the dispatcher, with the output "MOTIVE_POWER"
dsp.add_func(
    calc_MOTIVE_POWER, outputs=["MOTIVE_POWER", "f0_norm", "f1_norm", "f2_norm"]
)

# Add the function clean_ALTITUDE_RAW to the dispatcher, with the output "ALTITUDE"
dsp.add_func(clean_ALTITUDE_RAW, outputs=["ALTITUDE"])

# Add the function calc_ANGLE_SLOPE to the dispatcher, with the output "ANGLE_SLOPE"
dsp.add_func(process_ALTITUDE, outputs=["ANGLE_SLOPE", "PERCENT_SLOPE"])

# Add the function def_SPEED_from_DYNO_SPEED to the dispatcher, with the output "SPEED"
dsp.add_func(def_SPEED_from_DYNO_SPEED, outputs=["SPEED"])

# Add the function calc_f2_temp_ratio to the dispatcher, with the output "f2_temp_ratio"
dsp.add_func(calc_f2_temp_ratio, outputs=["f2_temp_ratio"])

# Add the function calc_mass_ratio_coc to the dispatcher, with the output "mass_ratio_coc"
dsp.add_func(calc_mass_ratio_coc, outputs=["mass_ratio_coc"])


if __name__ == "__main__":

    # Plot the dispatcher object
    dsp.plot()

    # Dispatch the inputs to the functions in the dispatcher object and store the solution
    sol = dsp.dispatch(
        inputs={
            "DYNO_SPEED": np.array([0, 100, 120]),
            "OBD_SPEED": np.array([0, 111, 125]),
            "GPS_SPEED": np.array([0, 99, 118]),
            "GPS_POS_VALID": 1,
        },
    )

    # Plot the solution
    sol.plot()
