import numpy as np
import schedula as sh


def def_rde_phase(SPEED):
    """
    Determine the phase of a trip based on the given speed.

    Parameters
    ----------
    SPEED: float, np.ndarray
        The speed value(s) for which the phase needs to be determined.

    Returns
    -------
    phase: str, np.ndarray
        The phase(s) of the trip corresponding to the given speed(s).
        Possible values are 'urban', 'rural', and 'motorway'.

    .. note::
    This function uses speed bins to determine the phase of a trip based on the given speed.
    The speed bins are defined as [0, 60, 90, 200], and the corresponding phases are mapped as follows:
    - Speeds <= 60 are classified as 'urban'
    - Speeds > 60 and <= 90 are classified as 'rural'
    - Speeds > 90 and <= 200 are classified as 'motorway'

    .. example::
    >>> def_rde_phase(50)
    'urban'

    >>> def_rde_phase([70, 100, 150])
    ['rural', 'motorway', 'motorway']
    """
    speed_bins = np.array([0, 60, 90, 200])
    map_phase = {1: "urban", 2: "rural", 3: "motorway"}
    inds = np.digitize(SPEED, speed_bins)
    return np.vectorize(map_phase.get)(inds)


def def_urban_phase(rde_phase):
    """
    Define the urban phase based on the given RDE phase.

    Parameters:
    -----------
    rde_phase: np.ndarray
        Array containing the RDE phase values.

    Returns:
    --------
    np.ndarray
        Array with the same shape as `rde_phase`, where each element is 1 if the corresponding
        element in `rde_phase` is "urban", and 0 otherwise.

    .. note::
    - The function assumes that the `rde_phase` array contains string values.
    - The function uses NumPy's `where` function to perform the element-wise comparison.

    .. example::
    >>> rde_phase = np.array(["urban", "rural", "urban", "urban"])
    >>> def_urban_phase(rde_phase)
    array([1, 0, 1, 1])
    """
    return np.where(rde_phase == "urban", 1, 0)


def def_rural_phase(rde_phase):
    """
    Convert the given RDE phase to a binary value indicating if it is rural or not.

    Parameters:
    -----------
    rde_phase: np.ndarray
        Array containing the RDE phase values.

    Returns:
    --------
    np.ndarray
        Array of binary values indicating if the RDE phase is rural (1) or not (0).

    .. note::
    - The function assumes that the input array contains string values.
    - The function uses the numpy.where() function to perform the conversion.

    .. example::
    >>> rde_phase = np.array(["urban", "rural", "urban", "rural"])
    >>> def_rural_phase(rde_phase)
    array([0, 1, 0, 1])
    """
    return np.where(rde_phase == "rural", 1, 0)


def def_motorway_phase(rde_phase):
    """
    Convert the given RDE phase array into a binary array indicating the presence of motorway phase.

    Parameters:
    -----------
    rde_phase: np.ndarray
        Array containing the RDE phases.

    Returns:
    --------
    np.ndarray
        Binary array indicating the presence of motorway phase.

    Notes:
    ------
    - The function assumes that the input array contains string values.
    - The function uses NumPy's `where` function to convert the array into a binary array.

    Example:
    --------
    >>> rde_phase = np.array(["motorway", "urban", "motorway", "rural"])
    >>> def_motorway_phase(rde_phase)
    array([1, 0, 1, 0])
    """
    return np.where(rde_phase == "motorway", 1, 0)


def cumulate_distance_in_urban(delta_DISTANCE, is_urban):
    """
    Cumulates the distance traveled in urban areas.

    Parameters:
    -----------
    delta_DISTANCE: np.ndarray
        Array of distances traveled between consecutive points.
    is_urban: np.ndarray
        Array of boolean values indicating whether each point is in an urban area.

    Returns:
    --------
    cumulated_distance : ndarray
        Array of cumulated distances in urban areas.

    .. note::
    - The `delta_DISTANCE` and `is_urban` arrays must have the same length.
    - The `is_urban` array should contain boolean values, where `True` indicates an urban area and `False` indicates a non-urban area.

    .. example::
    >>> delta_DISTANCE = np.array([10, 20, 15, 5, 30])
    >>> is_urban = np.array([True, False, True, False, True])
    >>> cumulate_distance_in_urban(delta_DISTANCE, is_urban)
    array([10, 10, 25, 25, 55])

    """
    return np.cumsum(delta_DISTANCE * is_urban)


def cumulate_distance_in_rural(delta_DISTANCE, is_rural):
    """
    Cumulates the distance traveled in rural areas.

    Parameters:
    -----------
    delta_DISTANCE: np.ndarray
        Array containing the distance traveled at each step.
    is_rural: np.ndarray
        Array indicating whether each step was in a rural area (1) or not (0).

    Returns:
    --------
    np.ndarray
        Array containing the cumulative distance traveled in rural areas.

    .. note::
    - The input arrays `delta_DISTANCE` and `is_rural` must have the same length.
    - The function uses element-wise multiplication to calculate the distance traveled in rural areas at each step.
    - The cumulative sum of the distances is then calculated using `np.cumsum()`.

    .. example::
    >>> delta_DISTANCE = np.array([10, 20, 15, 30])
    >>> is_rural = np.array([1, 0, 1, 1])
    >>> cumulate_distance_in_rural(delta_DISTANCE, is_rural)
    array([10, 10, 25, 55])
    """
    return np.cumsum(delta_DISTANCE * is_rural)


def cumulate_distance_in_motorway(delta_DISTANCE, is_motorway):
    """
    Cumulates the distance in the motorway based on the delta distance and motorway flags.

    Parameters
    ----------
    delta_DISTANCE: np.ndarray
        Array containing the delta distance values.
    is_motorway: np.ndarray
        Array of boolean values indicating whether each distance is in the motorway.

    Returns
    -------
    np.ndarray
        Array containing the cumulative distance in the motorway.

    .. note::
    - The `delta_DISTANCE` and `is_motorway` arrays must have the same length.
    - The `is_motorway` array should have boolean values (True or False).

    .. example::
    >>> delta_DISTANCE = np.array([100, 200, 150, 300])
    >>> is_motorway = np.array([True, False, True, True])
    >>> cumulate_distance_in_motorway(delta_DISTANCE, is_motorway)
    array([100, 100, 250, 550])
    """
    return np.cumsum(delta_DISTANCE * is_motorway)


def cumulate_fuel_kg_in_urban(delta_FUEL_g, is_urban):
    return np.cumsum(delta_FUEL_g * is_urban) / 1000


def cumulate_fuel_kg_in_rural(delta_FUEL_g, is_rural):
    return np.cumsum(delta_FUEL_g * is_rural) / 1000


def cumulate_fuel_kg_in_motorway(delta_FUEL_g, is_motorway):
    return np.cumsum(delta_FUEL_g * is_motorway) / 1000


def cumulate_hv_energy_in_urban(E_HVBAT_deltas_wh, is_urban):
    return np.cumsum(E_HVBAT_deltas_wh * is_urban) / 1000


def cumulate_hv_energy_in_rural(E_HVBAT_deltas_wh, is_rural):
    return np.cumsum(E_HVBAT_deltas_wh * is_rural) / 1000


def cumulate_hv_energy_in_motorway(E_HVBAT_deltas_wh, is_motorway):
    return np.cumsum(E_HVBAT_deltas_wh * is_motorway) / 1000


def cumulate_motor_energy_in_urban(E_MOTOR_deltas_wh, is_urban):
    return np.cumsum(E_MOTOR_deltas_wh * is_urban) / 1000


def cumulate_motor_energy_in_rural(E_MOTOR_deltas_wh, is_rural):
    return np.cumsum(E_MOTOR_deltas_wh * is_rural) / 1000


def cumulate_motor_energy_in_motorway(E_MOTOR_deltas_wh, is_motorway):
    return np.cumsum(E_MOTOR_deltas_wh * is_motorway) / 1000


def distance_share_in_urban(DISTANCE_urban, DISTANCE):
    """
    Calculate the share of urban distance in the total distance.

    Parameters:
    -----------
    DISTANCE_urban: np.ndarray
        Array of urban distances.
    DISTANCE: np.ndarray
        Array of total distances.

    Returns:
    --------
    np.ndarray
        Array of the share of urban distance in the total distance.

    .. note::
    - The function calculates the share of urban distance in the total distance by dividing
    each urban distance by the corresponding total distance.
    - If the total distance is less than or equal to 1, the share is set to 0.

    .. example::
    >>> DISTANCE_urban = np.array([2, 3, 4])
    >>> DISTANCE = np.array([5, 2, 6])
    >>> distance_share_in_urban(DISTANCE_urban, DISTANCE)
    array([0.4, 1.5, 0.66666667])
    """
    with np.errstate(divide="ignore", invalid="ignore"):
        result = np.where(DISTANCE > 1, DISTANCE_urban / DISTANCE, 0)
    return result


def distance_share_in_rural(DISTANCE_rural, DISTANCE):
    """
    Calculate the share of rural distance in the total distance.

    Parameters:
    -----------
    DISTANCE_rural: np.ndarray
        Array containing the rural distances.
    DISTANCE : np.ndarray
        Array containing the total distances.

    Returns:
    --------
    np.ndarray
        Array containing the share of rural distance in the total distance.

    .. note::
    - The function calculates the share of rural distance by dividing the rural distance by the total distance.
    - If the total distance is less than or equal to 1, the share is set to 0.

    .. example::
    >>> DISTANCE_rural = np.array([10, 20, 30])
    >>> DISTANCE = np.array([5, 10, 15])
    >>> distance_share_in_rural(DISTANCE_rural, DISTANCE)
    array([2. , 2. , 2. ])
    """
    with np.errstate(divide="ignore", invalid="ignore"):
        result = np.where(DISTANCE > 1, DISTANCE_rural / DISTANCE, 0)
    return result


def distance_share_in_motorway(DISTANCE_motorway, DISTANCE):
    """
    Calculate the share of distance in the motorway.

    Parameters:
    -----------
    DISTANCE_motorway: np.ndarray
        Array containing the distances in the motorway.
    DISTANCE : np.ndarray
        Array containing the total distances.

    Returns:
    --------
    np.ndarray
        Array containing the share of distance in the motorway.

    .. note::
    - The function calculates the share of distance in the motorway by dividing
    the distance in the motorway by the total distance.
    - If the total distance is less than or equal to 1, the share is set to 0.

    .. example::
    >>> DISTANCE_motorway = np.array([100, 200, 300])
    >>> DISTANCE = np.array([500, 1000, 1500])
    >>> distance_share_in_motorway(DISTANCE_motorway, DISTANCE)
    array([0.2, 0.2, 0.2])
    """
    with np.errstate(divide="ignore", invalid="ignore"):
        result = np.where(DISTANCE > 1, DISTANCE_motorway / DISTANCE, 0)
    return result


def avg_speed_in_urban(SPEED, is_urban):
    """
    Calculate the average speed in urban areas.

    Parameters:
    -----------
    SPEED: np.ndarray
        Array containing the speed values.
    is_urban: np.ndarray
        Array indicating whether each speed value corresponds to an urban area (1) or not (0).

    Returns:
    --------
    float
        The average speed in urban areas.

    .. note::
    - The SPEED and is_urban arrays must have the same length.
    - The is_urban array should contain binary values (0 or 1) indicating whether each speed value corresponds to an urban area or not.
    - The function uses numpy.mean() to calculate the average speed in urban areas.

    .. example::
    >>> SPEED = np.array([50, 60, 40, 55, 45])
    >>> is_urban = np.array([1, 0, 1, 1, 0])
    >>> avg_speed_in_urban(SPEED, is_urban)
    48.333333333333336
    """
    return np.mean(SPEED[is_urban == 1]) if sum(is_urban) > 0 else np.nan


def avg_speed_in_rural(SPEED, is_rural):
    """
    Calculate the average speed in rural areas.

    Parameters:
    -----------
    SPEED: np.ndarray
        Array containing the speeds.
    is_rural: np.ndarray
        Array indicating whether each speed corresponds to a rural area (1) or not (0).

    Returns:
    --------
    float
        Average speed in rural areas.

    .. note::
    - This function assumes that SPEED and is_rural have the same length.
    - The average speed is calculated only for the speeds corresponding to rural areas.

    .. example::
    >>> SPEED = np.array([50, 60, 70, 80, 90])
    >>> is_rural = np.array([1, 0, 1, 1, 0])
    >>> avg_speed_in_rural(SPEED, is_rural)
    66.66666666666667
    """
    return np.mean(SPEED[is_rural == 1]) if sum(is_rural) > 0 else np.nan


def avg_speed_in_motorway(SPEED, is_motorway):
    """
    Calculate the average speed in a motorway.

    Parameters:
    -----------
    SPEED: np.ndarray
        Array containing the speeds.
    is_motorway: np.ndarray
        Array indicating whether each speed corresponds to a motorway (1) or not (0).

    Returns:
    --------
    float
        Average speed in the motorway.

    .. note::
    - The function assumes that SPEED and is_motorway have the same length.
    - The function only considers speeds corresponding to motorways (is_motorway == 1).

    .. example::
    >>> SPEED = np.array([60, 70, 80, 90, 100])
    >>> is_motorway = np.array([0, 1, 1, 0, 1])
    >>> avg_speed_in_motorway(SPEED, is_motorway)
    83.33333333333333
    """
    return np.mean(SPEED[is_motorway == 1]) if sum(is_motorway) > 0 else np.nan


def calc_cumulative_time_ice_off(TIME, ice_speed):
    """
    Calculate the cumulative time when ice is off based on the given TIME and ice_speed.

    Parameters:
    ----------
    TIME: np.ndarray
        Array of time values.
    ice_speed: np.ndarray
        Array of ice speed values.

    Returns:
    -------
    np.ndarray
        Array of cumulative time values when ice is off.

    .. note::
    - The function assumes that the TIME and ice_speed arrays have the same length.
    - The cumulative time when ice is off is calculated by summing the time intervals
    when the ice speed is zero.

    .. example::
    >>> TIME = [0, 1, 2, 3, 4, 5]
    >>> ice_speed = [0, 0, 1, 0, 0, 1]
    >>> calc_cumulative_time_ice_off(TIME, ice_speed)
    array([0, 0, 1, 1, 1, 2])

    """
    return np.cumsum(np.diff(TIME, prepend=0) * np.where(ice_speed == 0, 1, 0))


def calc_CED(E_wheels_pos_cumulative):
    """
    Calculates the Cycle Energy Demand (CED) from a cycle or trip.

    Parameters:
    ----------
    E_wheels_pos_cumulative:  np.ndarray
        Array of cumulative high-voltage energy values in kWh.

    Returns:
    -------
    float
        The Cycle Energy Demand (CED) in Wh.
    """
    return E_wheels_pos_cumulative[-1] * 1000


def calc_REEC(delta_E_REESS, CED):
    """
    Calculates the Relative Electric Energy Change (REEC) from a cycle or trip.

    Parameters
    ----------
    E_HVBAT_cumulative: np.ndarray
        Array of cumulative high-voltage energy values in kWh.
    CED: float
        The Cycle Energy Demand (CED) in Wh.

    Returns
    -------
    float
        Relative Electric Energy Change [-] calculated according to 2017/1151 or UN154.

    """
    return delta_E_REESS / CED * 1000


def calc_urban_average_motor_ec(E_MOTOR_cumulative_urban, DISTANCE_urban):
    return E_MOTOR_cumulative_urban / DISTANCE_urban * 1000


def calc_rural_average_motor_ec(E_MOTOR_cumulative_rural, DISTANCE_rural):
    return E_MOTOR_cumulative_rural / DISTANCE_rural * 1000


def calc_motorway_average_motor_ec(E_MOTOR_cumulative_motorway, DISTANCE_motorway):
    return E_MOTOR_cumulative_motorway / DISTANCE_motorway * 1000


# Create a dispatcher object named "dsp"
dsp = sh.Dispatcher(name="dsp")

# Add functions to the dispatcher with specified outputs
dsp.add_func(def_rde_phase, outputs=["rde_phase"])
dsp.add_func(def_urban_phase, outputs=["is_urban"])
dsp.add_func(def_rural_phase, outputs=["is_rural"])
dsp.add_func(def_motorway_phase, outputs=["is_motorway"])
dsp.add_func(cumulate_distance_in_urban, outputs=["DISTANCE_urban"])
dsp.add_func(cumulate_distance_in_rural, outputs=["DISTANCE_rural"])
dsp.add_func(cumulate_distance_in_motorway, outputs=["DISTANCE_motorway"])
dsp.add_func(cumulate_fuel_kg_in_urban, outputs=["FUEL_kg_urban"])
dsp.add_func(cumulate_fuel_kg_in_rural, outputs=["FUEL_kg_rural"])
dsp.add_func(cumulate_fuel_kg_in_motorway, outputs=["FUEL_kg_motorway"])
dsp.add_func(cumulate_hv_energy_in_urban, outputs=["E_HVBAT_cumulative_urban"])
dsp.add_func(cumulate_hv_energy_in_rural, outputs=["E_HVBAT_cumulative_rural"])
dsp.add_func(cumulate_hv_energy_in_motorway, outputs=["E_HVBAT_cumulative_motorway"])
dsp.add_func(cumulate_motor_energy_in_urban, outputs=["E_MOTOR_cumulative_urban"])
dsp.add_func(cumulate_motor_energy_in_rural, outputs=["E_MOTOR_cumulative_rural"])
dsp.add_func(cumulate_motor_energy_in_motorway, outputs=["E_MOTOR_cumulative_motorway"])
dsp.add_func(distance_share_in_urban, outputs=["share_urban"])
dsp.add_func(distance_share_in_rural, outputs=["share_rural"])
dsp.add_func(distance_share_in_motorway, outputs=["share_motorway"])
dsp.add_func(avg_speed_in_urban, outputs=["avg_SPEED_urban"])
dsp.add_func(avg_speed_in_rural, outputs=["avg_SPEED_rural"])
dsp.add_func(avg_speed_in_motorway, outputs=["avg_SPEED_motorway"])
dsp.add_func(calc_urban_average_motor_ec, outputs=["urban_average_motor_ec"])
dsp.add_func(calc_rural_average_motor_ec, outputs=["rural_average_motor_ec"])
dsp.add_func(calc_motorway_average_motor_ec, outputs=["motorway_average_motor_ec"])
dsp.add_func(calc_cumulative_time_ice_off, outputs=["cumulative_time_ice_off"])
dsp.add_func(calc_CED, outputs=["CED"])
dsp.add_func(calc_REEC, outputs=["REEC"])


if __name__ == "__main__":
    dsp.plot()
