def calc_P_el_from_V_I(V, I):
    """
    Calculates electric power from voltage and current.

    Parameters
    ----------
    V: float, np.ndarray
        Voltage [V].
    I: float, np.ndarray
        Current [A].

    Returns
    -------
    float, np.ndarray
        Power [kW].

    .. note::
    This function assumes that the voltage and current values are in the correct units (V and A, respectively).
    The power is calculated using the formula:
    .. math::
        \text{{power (kW)}} = \frac{{\text{{voltage (V)}} \times \text{{current (A)}}}}{{1000}}
    
    The power is returned in kilowatts (kW).
    
    .. example::
    >>> calc_P_el_from_V_I(230, 5)
    1.15
    >>> calc_P_el_from_V_I(120, 2.5)
    0.3
    """
    return V * I / 1000


def calc_V_from_P_el_I(P, I):
    """
    Calculate voltage (V) from electrical power (P) and current (I).

    Parameters:
    ----------
    P: float, np.ndarray
        Electrical power in watts.
    I: float, np.ndarray
        Current in amperes.

    Returns:
    -------
    float, np.ndarray
        Voltage in volts.

    .. note::
    This function assumes a linear relationship between power, current, and voltage.
    The formula used is:
    .. math::
        V = \frac{P}{I} \times 1000


    .. example::
    >>> calc_V_from_P_el_I(1000, 10)
    100.0
    """
    return P / I * 1000


import numpy as np


def calc_P_me_from_T_S(T, S):
    """
    Calculate the mechanical power (P_me) from torque (T) and speed (S).

    Parameters:
    -----------
    T: float, np.ndarray
        Torque in Nm.
    S: float, np.ndarray
        Speed in rpm.

    Returns:
    --------
    float, np.ndarray
        Mechanical power in kW.

    .. note::
    - Torque (T) should be given in Newton meters (Nm).
    - Speed (S) should be given in revolutions per minute (rpm).
    - The mechanical power (P_me) is calculated using the formula:
    .. math::
        P_{me} = \frac{T \times 2 \pi \times S}{60 \times 1000}

    .. example::
    >>> calc_P_me_from_T_S(100, 2000)
    20.94395102393195
    """
    return T * 2 * np.pi * S / 60 / 1000


def calc_S_from_P_me_T(P_me, T):
    """
    Calculate the speed (S) from mechanical power (P_me) and torque (T).

    Parameters:
    ----------
    P_me: float, np.ndarray
        The mechanical power in kW.
    T: float, np.ndarray
        The torque in Nm.

    Returns:
    -------
    float
        The calculated value of S.

    .. note::
    - The formula used to calculate speed:
    .. math::
        S = \frac{P_{me}}{T \times 2 \pi} \times 60 \times 1000
    - The np.errstate is used to ignore divide and invalid errors.

    .. example::
    >>> calc_S_from_P_me_T(10, 5)
    318.3098861837907
    """
    with np.errstate(divide="ignore", invalid="ignore"):
        return P_me / (2 * np.pi * T) * 60 * 1000


def calc_P_me_from_P_el(P_el, eff_me_to_el):
    """
    Calculate the mechanical power from electrical power.

    Parameters:
    -----------
    P_el: float, np.ndarray
        Electrical power [kW].
    eff_me_to_el: float
        Efficiency of the mechanical-to-electrical conversion.

    Returns:
    --------
    float
        Mechanical power in kilo-watts (kW).

    .. note::
    - The mechanical power is calculated by multiplying the electrical power by the efficiency
    of the mechanical-to-electrical conversion:
    .. math::
        P_{me} = P_{el} \times \text{{efficiency of mechanical-to-electrical conversion}}

    - The sign of the electrical power is preserved in the calculation.

    .. example::
    >>> calc_P_me_from_P_el(100, 0.9)
    90.0
    """
    return P_el * eff_me_to_el ** (np.sign(P_el))


def calc_P_el_from_P_me(P_me, eff_el_to_me):
    """
    Calculate the electrical power from the mechanical power.

    Parameters:
    -----------
    P_me: float, np.ndarray
        The mechanical power.
    eff_el_to_me: float
        The efficiency of the electrical to mechanical power conversion.

    Returns:
    --------
    float
        The electrical power in kilo-watts (kW).

    Notes:
    ------
    - The electrical power is calculated by multiplying the mechanical power by the efficiency
    of the electrical to mechanical power conversion:
    .. math::
        P_{el} = P_{me} \times \text{{efficiency of electrical-to-mechanical conversion}}
    - The sign of the mechanical power is preserved in the calculation.

    Example:
    --------
    >>> calc_P_el_from_P_me(100, 0.9)
    90.0
    """
    return P_me * eff_el_to_me ** (np.sign(P_me))
