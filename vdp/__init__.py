import schedula as sh

from vdp.time import dsp as time
from vdp.fuel import dsp as fuel
from vdp.components.ice import dsp as ice
from vdp.components.transmission import dsp as transmission
from vdp.components.wheels import dsp as wheels
from vdp.components.drivebattery import dsp as drivebattery
from vdp.components.servicebattery import dsp as servicebattery
from vdp.components.emotors import dsp as motor
from vdp.components.mac import dsp as mac
from vdp.components.dcdc import dsp as dcdc
from vdp.components.esystem import dsp as esystem
from vdp.dynamics import dsp as dynamics
from vdp.energy import dsp as energy
from vdp.efficiency import dsp as efficiency
from vdp.trip import dsp as trip
from vdp.gps import dsp as gps

from vdp.defaults import dict_defaults


# Create a dispatcher object named "vdp"
vdp_dsp = sh.Dispatcher(name="vdp")

# Extend the dispatcher adding vehicle components and other calculation modules
vdp_dsp.extend(
    time,
    fuel,
    ice,
    transmission,
    wheels,
    drivebattery,
    servicebattery,
    motor,
    mac,
    dcdc,
    esystem,
    dynamics,
    energy,
    efficiency,
    trip,
    gps,
)

for k, v in dict_defaults.items():
    vdp_dsp.set_default_value(k, value=v["value"], initial_dist=v["initial_dist"])

# If this file is executed as the main script, plot the dispatcher
if __name__ == "__main__":
    import numpy as np

    sol = vdp_dsp.dispatch(
        inputs={
            "TIME": np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
            "SPEED": np.array([0, 2, 4, 5, 5, 6, 7, 10, 10, 11, 12]),
            "T_AMB": np.array([1, 1, 1, 1, 1, 1, 1, 1, 1, 1]),
            "test_mass_coc": 1700,
            "veh_type": "ev",
            "f0": 100,
            "f1": 1,
            "f2": 0.03,
            "test_mass": 1800,
        }
    )
    vdp_dsp.plot()
    # sol.plot()
