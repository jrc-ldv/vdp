import numpy as np
from vdp.algorithms import signal_is_frozen
import schedula as sh
from scipy.interpolate import interp1d


def def_default_GPS_ALT(TIME):
    """
    The function returns an array of zeros with the same shape as the input array TIME.

    Parameters
    ----------
    TIME: np.ndarray
        Array that represents a sequence of time values.

    Returns
    -------
    np.ndarray
        An array of zeros with the same shape as the input variable TIME.

    .. note::
    This function is used to initialize the GPS altitude values to zero.

    .. example::
    >>> time = np.array([1, 2, 3, 4, 5])
    >>> def_default_GPS_ALT(time)
    array([0, 0, 0, 0, 0])
    """
    return np.zeros_like(TIME)


def def_GPS_horizontal_accuracy_ok(
    TIME, GPS_LAT, GPS_LON, GPS_HOR_ACC, GPS_SPEED_ACC, GPS_SAT, GPS_HDOP
):
    """
    Check various conditions related to GPS data and determine whether the GPS horizontal accuracy is considered good or not.

    Parameters
    ----------
    TIME: np.ndarray
        Time at which the GPS measurements were taken.
    GPS_LAT: np.ndarray
        The latitude coordinates obtained from the GPS signal.
    GPS_LON: np.ndarray
        The longitude coordinates obtained from the GPS signal.
    GPS_HOR_ACC: np.ndarray
        GPS horizontal accuracy, measured in meters. It represents the estimated accuracy of the GPS
        position in the horizontal plane.
    GPS_SPEED_ACC: np.ndarray
        The accuracy of the GPS speed measurement. It is used to determine if the GPS
        speed reading is accurate or not.
    GPS_SAT: np.ndarray
        The number of satellites used for GPS positioning.
    GPS_HDOP: np.ndarray
        GPS HDOP (Horizontal Dilution of Precision) is a measure of the accuracy of the GPS horizontal
        position. It indicates the quality of the satellite geometry and the signal strength. A higher HDOP
        value indicates a lower accuracy.

    Returns
    -------
    np.ndarray
        Array of integers indicating whether the GPS horizontal accuracy is considered okay or not. The
        value 1 indicates that the GPS horizontal accuracy is okay, while the value 0 indicates that it is
        not okay.

    .. note::
    This function checks the following conditions:
    - GPS measurements were taken for at least 180 seconds.
    - The latitude and longitude signals are not frozen.
    - GPS horizontal accuracy is not greater than 100 meters.
    - GPS speed accuracy is not worse than 10 km/h off.
    - The number of satellites used for GPS positioning is at least 5.
    - GPS HDOP is not greater than 50.

    .. example::
    >>> TIME = np.array([0, 60, 120, 180, 240])
    >>> GPS_LAT = np.array([51.5074, 51.5074, 51.5074, 51.5074, 51.5074])
    >>> GPS_LON = np.array([-0.1278, -0.1278, -0.1278, -0.1278, -0.1278])
    >>> GPS_HOR_ACC = np.array([10, 20, 30, 40, 50])
    >>> GPS_SPEED_ACC = np.array([2, 4, 6, 8, 10])
    >>> GPS_SAT = np.array([5, 6, 7, 8, 9])
    >>> GPS_HDOP = np.array([1, 2, 3, 4, 5])
    >>> def_GPS_horizontal_accuracy_ok(TIME, GPS_LAT, GPS_LON, GPS_HOR_ACC, GPS_SPEED_ACC, GPS_SAT, GPS_HDOP)
    array([1, 1, 1, 1, 1])
    """

    # Check conditions
    GPS_active_not_long_enough = TIME < 180
    GPS_LAT_frozen = signal_is_frozen(TIME, GPS_LAT)
    GPS_LON_frozen = signal_is_frozen(TIME, GPS_LON)
    if GPS_HOR_ACC is not None:
        GPS_HOR_ACC_bad = GPS_HOR_ACC > 100000  # 100 meters
    else:
        GPS_HOR_ACC_bad = np.zeros_like(TIME, dtype=bool)
    if GPS_HOR_ACC is not None:
        GPS_SPEED_ACC_bad = GPS_SPEED_ACC > 10 / 3.6  # worse than km/h off
    else:
        GPS_SPEED_ACC_bad = np.zeros_like(TIME, dtype=bool)
    if GPS_SAT is not None:
        GPS_SAT_too_few = GPS_SAT < 5
    else:
        GPS_SAT_too_few = np.zeros_like(TIME, dtype=bool)
    if GPS_HDOP is not None:
        GPS_HDOP_bad = GPS_HDOP > 50
    else:
        GPS_HDOP_bad = np.zeros_like(TIME, dtype=bool)

    # Combine conditions
    GPS_bad = np.column_stack(
        [
            GPS_active_not_long_enough,
            GPS_LAT_frozen,
            GPS_LON_frozen,
            GPS_HOR_ACC_bad,
            GPS_SPEED_ACC_bad,
            GPS_SAT_too_few,
            GPS_HDOP_bad,
        ]
    ).any(axis=1)

    print("GPS horizontal accuracy: bad readings were rejected.")

    return np.array(np.logical_not(GPS_bad)).astype(int)


def def_GPS_ALT_VALID_from_altitude_sats_hdop(TIME, GPS_ALT, GPS_SAT, GPS_HDOP):
    """
    Check various conditions related to GPS altitude, satellite count, and HDOP to determine if the GPS readings are valid.

    Parameters
    ----------
    TIME: np.ndarray
        Time values for the GPS readings.
    GPS_ALT: np.ndarray
        Altitude readings from the GPS.
    GPS_SAT: np.ndarray
        Number of satellites detected by the GPS system.
    GPS_HDOP: np.ndarray
        GPS HDOP (Horizontal Dilution of Precision) is a measure of the accuracy of the GPS horizontal
        position. It indicates the amount of error in the GPS position due to satellite geometry. A lower
        HDOP value indicates better accuracy.

    Returns
    -------
    np.ndarray
        Array which represents whether the GPS readings are considered valid or not.

    .. note::
    This function checks the following conditions to determine if the GPS readings are valid:
    1. If the GPS altitude readings are available.
    2. If the GPS has been active for at least 180 seconds.
    3. If the GPS altitude readings are frozen.
    4. If the number of satellites detected by the GPS is less than 5.
    5. If the GPS HDOP value is greater than 50.

    .. example::
    >>> TIME = np.array([0, 60, 120, 180, 240])
    >>> GPS_ALT = np.array([100, 150, 200, 250, 300])
    >>> GPS_SAT = np.array([8, 6, 4, 2, 0])
    >>> GPS_HDOP = np.array([10, 20, 30, 40, 50])
    >>> def_GPS_ALT_VALID_from_altitude_sats_hdop(TIME, GPS_ALT, GPS_SAT, GPS_HDOP)
    array([1, 1, 1, 0, 0])
    """
    if True:  # GPS_ALT.any():
        GPS_active_not_long_enough = np.where(TIME < 180, 1, 0)
        GPS_ALT_frozen = signal_is_frozen(TIME, GPS_ALT)
        if GPS_SAT is not None:
            GPS_SAT_too_few = GPS_SAT < 5
        else:
            GPS_SAT_too_few = np.zeros_like(TIME)
        if GPS_HDOP is not None:
            GPS_HDOP_bad = GPS_HDOP > 50
        else:
            GPS_HDOP_bad = np.zeros_like(TIME)

        GPS_bad = np.column_stack(
            [
                # GPS_active_not_long_enough,
                GPS_ALT_frozen,
                GPS_SAT_too_few,
                GPS_HDOP_bad,
            ]
        ).any(axis=1)

        GPS_ok = np.array(np.logical_not(GPS_bad)).astype(int)

        print("GPS vertical accuracy: bad readings were rejected.")
    else:
        GPS_ok = np.ones_like(TIME, dtype=bool)

    return GPS_ok


def def_GPS_ALT_VALID_from_altitude(TIME, GPS_ALT):
    """
    Calculate the validity of GPS altitude readings based on altitude gradient.

    This function returns an array of zeros with the same shape as the time signal,
    indicating the validity of GPS altitude readings based on the altitude gradient.
    Altitude readings are considered valid if the absolute value of the altitude gradient
    is less than a threshold value and the altitude is not equal to zero.

    Parameters
    ----------
    TIME: np.ndarray
        Time values for the GPS readings.
    GPS_ALT: np.ndarray
        The altitude data from a GPS sensor.

    Returns
    -------
    np.ndarray
        Array of zeros with the same shape as the input array TIME, indicating the
        validity of GPS altitude readings.

    .. note::
    The altitude gradient is calculated using the numpy.gradient function.
    Altitude readings with an absolute gradient value less than the threshold are
    considered valid.

    .. example::
    >>> TIME = np.array([0, 1, 2, 3, 4])
    >>> GPS_ALT = np.array([0, 10, 20, 30, 40])
    >>> def_GPS_ALT_VALID_from_altitude(TIME, GPS_ALT)
    array([0, 0, 0, 0, 0])

    In this example, all the GPS altitude readings are considered invalid, as the
    altitude gradient is zero for all time points and the altitude is not equal to zero.
    """

    thr = 0.6
    alt_grad = np.gradient(GPS_ALT, TIME)
    select = (abs(alt_grad) < thr) & (GPS_ALT != 0)
    return np.where(select, 1, 0)


def clean_GPS_ALT(TIME, GPS_ALT, GPS_ALT_VALID):
    """
    Interpolates the GPS altitude values based on the valid GPS altitude
    values and returns the interpolated values for all time points.

    Parameters
    ----------
    TIME: np.ndarray
        The time values for the GPS altitude measurements.
    GPS_ALT: np.ndarray
        The GPS altitude values.
    GPS_ALT_VALID: np.ndarray
        Boolean array indicating whether each GPS_ALT value is valid or not.
        A value of True indicates that the GPS_ALT value is valid, while a
        value of False indicates that the GPS_ALT value is invalid.

    Returns
    -------
    np.ndarray
        The interpolated GPS altitude values based on the given time values.

    .. note::
    This function performs interpolation on the GPS altitude values using
    scipy.interpolate.interp1d. It selects only the valid GPS altitude values
    and interpolates them to obtain altitude values for all time points.

    .. example::
    >>> TIME = np.array([0, 1, 2, 3, 4])
    >>> GPS_ALT = np.array([10, 20, 30, 40, 50])
    >>> GPS_ALT_VALID = np.array([True, True, False, True, True])
    >>> clean_GPS_ALT(TIME, GPS_ALT, GPS_ALT_VALID)
    array([10., 20., 30., 35., 40.])
    """

    if (GPS_ALT.any()) & (GPS_ALT_VALID.any()):
        select = GPS_ALT_VALID
        t = TIME[np.where(select == 0, False, True)]
        a = GPS_ALT[np.where(select == 0, False, True)]
        if len(a) >= 2:
            # quadratic and cubic introduce big variations when GPS data is frozen, using linear
            return interp1d(t, a, bounds_error=False, fill_value=(a[0], a[-1]), kind='linear')(TIME)
    else:
        # No valid data to interpolate, returning error value for altitude
        return np.ones_like(TIME) * (-999)


def get_srtm_elevation(TIME, GPS_LAT, GPS_LON):

    """
    Retrieves elevation data from SRTM based on GPS coordinates and returns the elevation values.

    Parameters
    ----------
    TIME: np.ndarray
        The timestamps of the GPS data points.
    GPS_LAT: np.ndarray
        GPS latitude coordinates.
    GPS_LON: np.ndarray
        The longitude coordinates of the GPS locations.

    Returns
    -------
    np.ndarray or tuple
        If return_distances is False, it returns the cleaned altitude data as np.ndarray.
        If return_distances is True, it returns a tuple containing the distances (np.ndarray)
        and the cleaned altitude data (np.ndarray).

    Notes
    -----
    This function retrieves elevation data from SRTM (Shuttle Radar Topography Mission) based on
    the given GPS coordinates. It uses the `srtm.main.get_data` function to fetch the elevation data.
    The function then creates a GPX file with the provided GPS coordinates and adds the elevation
    data to the GPX file using the `srtm.main.get_data.add_elevations` method. Finally, it extracts
    the cleaned altitude data from the GPX file and returns it.

    Example
    -------
    >>> TIME = np.array([0, 1, 2, 3, 4])
    >>> GPS_LAT = np.array([51.5074, 52.5200, 48.8566, 40.7128, 37.7749])
    >>> GPS_LON = np.array([-0.1278, 13.4050, 2.3522, -74.0060, -122.4194])
    >>> get_srtm_elevation(TIME, GPS_LAT, GPS_LON, smooth=True, return_distances=False)
    array([  0.        ,  10.        ,  20.        ,  30.        ,  40.        ])
    """
    from srtm.main import get_data as srtm_get_data
    import gpxpy

    def func(lat, lon):
        xml = [
            '<gpx version="1.1" creator="GPS Visualizer http://www.gpsvisualizer.com/" xmlns="http://www.topografix.com/GPX/1/1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">'
        ]
        xml.append("\n<trk>\n")
        xml.append("  <name>lat_lon_test</name>\n  <trkseg>\n")
        for values in zip(lat, lon):
            xml.append(
                '    <trkpt {0}="{1}" {2}="{3}">'.format(
                    "lat", str(values[0]), "lon", str(values[1])
                )
            )
            xml.append("\n    </trkpt>\n")
        xml.append("  </trkseg>\n</trk>\n</gpx>")
        return "".join(xml)

    def get_line(gpx):
        previous_point = None
        length = 0
        lengths, elevations = [], []
        for point in gpx.walk(only_points=True):
            if previous_point:
                length += previous_point.distance_2d(point)
            previous_point = point
            lengths.append(length)
            elevations.append(point.elevation)
        return lengths, elevations

    if GPS_LAT.any():
        tt = func(GPS_LAT, GPS_LON)
        data = srtm_get_data()
        gpx = gpxpy.parse(tt)
        data.add_elevations(gpx, smooth=True)
        distances, altitudes = get_line(gpx)

        return altitudes
    else:
        return np.zeros_like(GPS_LAT)


def define_altitude_from_gps(GPS_ALT_CLEAN):
    return GPS_ALT_CLEAN


def define_altitude_from_srtm(ALTITUDE_SRTM):
    return ALTITUDE_SRTM


# The code snippet creates a `Dispatcher` object named `dsp` from the `schedula` library. The
# `Dispatcher` is used to define a sequence of functions that will be executed in a specific order
# based on the input data available.
dsp = sh.Dispatcher(name="gps")
dsp.add_func(def_GPS_ALT_VALID_from_altitude_sats_hdop, outputs=["GPS_ALT_VALID"])
dsp.add_func(def_GPS_ALT_VALID_from_altitude, outputs=["GPS_ALT_VALID"], weight=1)
dsp.add_func(def_default_GPS_ALT, outputs=["GPS_ALT"], weight=10)
dsp.add_func(def_GPS_horizontal_accuracy_ok, outputs=["GPS_POS_VALID"])
dsp.add_func(clean_GPS_ALT, outputs=["GPS_ALT_CLEAN"])
dsp.add_func(get_srtm_elevation, outputs=["ALTITUDE_SRTM"])
dsp.add_func(define_altitude_from_gps, outputs=["ALTITUDE_RAW"])
dsp.add_func(define_altitude_from_srtm, outputs=["ALTITUDE_RAW"], weight=3)

if __name__ == "__main__":
    # The `dsp.plot()` function is used to visualize the sequence of functions defined in the
    # `Dispatcher` object `dsp`. It generates a plot that shows the flow of data between the
    # functions, indicating the inputs and outputs of each function. This plot helps in understanding
    # the data dependencies and the order in which the functions will be executed.
    dsp.plot()
