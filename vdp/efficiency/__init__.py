import schedula as sh


# TODO: document code
def calc_SAE_extended_vehicle_efficiency(
    positive_traction_energy,
    m_fuel,
    hv_fuel,
    vehicle_mass,
    el_energy=0,
    initial_altitude=0,
    final_altitude=0,
    initial_speed=0,
    final_speed=0,
):
    # positive_traction_energy in kWh
    # m_fuel in kg
    # hv_fuel in kj/kg
    # el_energy in kWh, negative values when battery is being discharged
    # vehicle_mass in kg
    # initial_altitude and final_altitude in m
    # initial_speed and final_speed, initial and final vehicle speed in km/h

    fuel_energy = m_fuel * hv_fuel / 1000 / 3600
    electrical_energy = -el_energy
    potential_energy = (
        vehicle_mass * (final_altitude - initial_altitude) * 9.81 / 3600 / 1000
    )
    kinetic_energy = (
        -0.5
        * vehicle_mass
        * ((final_speed / 3.6) ** 2 - (initial_speed / 3.6) ** 2)
        / 3600
        / 1000
    )
    vehicle_energy_balance = (
        fuel_energy + electrical_energy + potential_energy + kinetic_energy
    )

    return positive_traction_energy / vehicle_energy_balance


dsp = sh.Dispatcher(name="dsp")
dsp.add_func(
    calc_SAE_extended_vehicle_efficiency, outputs=["SAE_extended_vehicle_efficiency"]
)

if __name__ == "__main__":
    dsp.plot()
