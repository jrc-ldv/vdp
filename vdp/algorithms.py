import numpy as np
import pandas as pd
from scipy.interpolate import InterpolatedUnivariateSpline as Spl
from scipy.interpolate import pchip
from scipy.integrate import cumulative_trapezoid


def calc_cumulative(times, values, initial=0):
    """Cumulatively integrate values using the composite trapezoidal rule.

    Parameters
    ----------
    times : ndarray
        Time to integrate along.
    values : ndarray
        Values to integrate.
    initial : int, optional
        If given, insert this value at the beginning of the returned result, by default 0.

    Returns
    -------
    ndarray
        The result of cumulative integration of values.

    .. note::
        See completely documentation of it: `Scipy <https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.cumulative_trapezoid.html#scipy-integrate-cumulative-trapezoid>`__
    """
    return cumulative_trapezoid(values, times, initial=initial)


def calc_one_sided_cumulative(times, values, sign, initial=0):
    """Cumulatively integrate values using the composite trapezoidal rule.

    Parameters
    ----------
    times : ndarray
        Time to integrate along.
    values : ndarray
        Values to integrate.
    initial : int, optional
        If given, insert this value at the beginning of the returned result, by default 0.

    Returns
    -------
    ndarray
        The result of cumulative integration of values.

    .. note::
        See completely documentation of it: `Scipy <https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.cumulative_trapezoid.html#scipy-integrate-cumulative-trapezoid>`__
    """

    upsampled_times = np.linspace(
        min(times), max(times), len(times) * 10 + 1, endpoint=True
    )
    upsampled_times = np.sort(np.unique(np.append(upsampled_times, times)))

    from scipy.interpolate import interp1d

    f = interp1d(times, values, bounds_error=False, fill_value=(values[0], values[-1]))
    upsampled_values = f(upsampled_times)
    if sign == 1:
        upsampled_values = np.where(upsampled_values > 0, upsampled_values, 0)
    else:
        upsampled_values = np.where(upsampled_values < 0, upsampled_values, 0)

    upsampled_cumulative = cumulative_trapezoid(
        upsampled_values, upsampled_times, initial=initial
    )

    f_cum = interp1d(
        upsampled_times,
        upsampled_cumulative,
        bounds_error=False,
        fill_value=(upsampled_values[0], upsampled_values[-1]),
    )

    return f_cum(times)


def calc_energy_shares_timeseries(E_cumulative, E_cumulative_total):
    """Calculate energy shares time series

    Parameters
    ----------
    E_cumulative : ndarray
        Cumulative energy [].
    E_cumulative_total : ndarray
        Total cumulative energy [].

    Returns
    -------
    ndarray
        _description_
    """
    with np.errstate(divide="ignore", invalid="ignore"):
        return np.where(
            np.abs(E_cumulative_total) > 0,
            np.abs(E_cumulative) / np.abs(E_cumulative_total),
            0,
        )


def signal_is_frozen(times, values):
    """Signal frozen detection.

    Parameters
    ----------
    times : ndarray
        Time to evaluate along.
    values : ndarray
        Values to detect.

    Returns
    -------
    ndarray
        boolean array with frozen points.
    """
    threshold = 2  # seconds
    equals = np.where(values == np.append(0, values[0:-1]), 1, 0)
    ups = np.where(np.append(0, np.diff(equals)) == 1)[0]
    downs = np.where(np.append(0, np.diff(equals)) == -1)[0]
    times_up = times[ups]
    times_down = times[downs]
    deltas = [
        idx
        for idx, (t1, t0) in enumerate(zip(times_down, times_up))
        if (t1 - t0) >= threshold
    ]
    is_frozen = np.zeros_like(times)
    for idx in deltas:
        is_frozen = np.where(
            np.logical_and(times >= times_up[idx], times < times_down[idx]),
            1,
            is_frozen,
        )

    return is_frozen


def identify_changed_value_in_discretised_signal(values):
    changed_value_f = np.array(
        [
            True if np.round(current, 1) != np.round(previous, 1) else False
            for current, previous in zip(values[1:], values[:-1])
        ]
    )
    changed_value_f = np.concatenate((np.array([False]), changed_value_f))
    changed_value_i_f = np.where(changed_value_f)[0]

    return changed_value_f, changed_value_i_f


def calculate_trusted_values_in_discretised_signal(time_f, values_f):
    changed_value_f, changed_value_i_f = identify_changed_value_in_discretised_signal(
        values_f
    )

    in_between_time_f = time_f[:-1] - np.diff(time_f) / 2
    new_time_f = in_between_time_f[changed_value_i_f]
    current_f = values_f[changed_value_i_f]
    previous_f = values_f[changed_value_i_f - 1]
    real_f = (previous_f + current_f) / 2

    return new_time_f, real_f, changed_value_f, changed_value_i_f


def calc_anchor_points(
    consecutive_min_time, consecutive_max_time, consecutive_value, condition, thr
):
    all_new_times = []
    all_new_values = []

    for min_time in consecutive_min_time[condition].unique():
        mask = consecutive_min_time == min_time
        speed_value = consecutive_value[mask].min()
        max_time = consecutive_max_time[mask].max()
        division = max(3, int(np.ceil((max_time - min_time) / (thr / 2))))
        new_times = list(np.linspace(min_time, max_time, division)[1:-1])
        new_values = [speed_value] * len(new_times)
        all_new_times += new_times
        all_new_values += new_values

    return all_new_times, all_new_values


def add_anchor_points_speed(time_f, values_f, time_real_f, values_real_f):
    thr_low_high_speed = 5

    thr_high_speed = (
        2.5  # min consecutive time for which anchors are added at high speeds
    )
    thr_low_speed = 1  # min consecutive time for which anchors are added at low speeds

    ts_time_f = pd.Series(time_f)
    ts_values_f = pd.Series(values_f)

    consecutive_value = ts_values_f.groupby(
        (ts_values_f != ts_values_f.shift()).cumsum()
    ).transform("max")
    consecutive_min_time = ts_time_f.groupby(
        (ts_values_f != ts_values_f.shift()).cumsum()
    ).transform("min")
    consecutive_max_time = ts_time_f.groupby(
        (ts_values_f != ts_values_f.shift()).cumsum()
    ).transform("max")
    consecutive_time_delta = consecutive_max_time - consecutive_min_time

    all_new_times = []
    all_new_values = []

    conditions = (
        ((consecutive_time_delta >= thr_high_speed) & (values_f > thr_low_high_speed)),
        ((consecutive_time_delta >= thr_low_speed) & (values_f <= thr_low_high_speed)),
    )
    thrs = (thr_high_speed, thr_low_speed)

    for condition, thr in zip(conditions, thrs):
        new_times, new_values = calc_anchor_points(
            consecutive_min_time,
            consecutive_max_time,
            consecutive_value,
            condition,
            thr,
        )
        all_new_times += new_times
        all_new_values += new_values

    all_times = np.append(time_real_f, all_new_times)
    all_values = np.append(values_real_f, all_new_values)
    sorting = all_times.argsort()

    return all_times[sorting], all_values[sorting]


def calculate_trusted_values_in_vehicle_speed_signal(time, speed):

    (
        trusted_time,
        trusted_speed,
        changed_value,
        changed_value_i,
    ) = calculate_trusted_values_in_discretised_signal(time, speed)
    final_time, final_speed = add_anchor_points_speed(
        time, speed, trusted_time, trusted_speed
    )

    return final_time, final_speed


def calculate_smooth_speed_from_trusted_values(time, speed, time_us):

    thr_low_speed = 5  # below this apply pchip interp. to avoid overshooting in negatives when reaching standstill

    trusted_time, trusted_speed = calculate_trusted_values_in_vehicle_speed_signal(
        time, speed
    )
    final_speed_spline = Spl(trusted_time, trusted_speed)(time_us)
    final_speed_pchip = pchip(trusted_time, trusted_speed)(time_us)
    final_speed = np.where(
        final_speed_pchip < thr_low_speed, final_speed_pchip, final_speed_spline
    )

    return final_speed, trusted_time, trusted_speed


if __name__ == "__main__":
    t = np.arange(0, 10, 1)
    v = np.array([0, 1, 1, 1, 2, 2, 2, 2, 3, 3])
    print(signal_is_frozen(t, v))
