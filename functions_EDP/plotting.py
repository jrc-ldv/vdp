from plotly.offline import plot
import plotly.graph_objs as go


def create_simple_interactive_plot_from_df(df, filepath, auto_open=False):

    times = df.iloc[:, 0].values

    data = []
    x = -1

    for col, values in df.items():
        x += 1
        if x == 0:
            continue
        globals()["trace%s" % x] = go.Scatter(x=times, y=values, name=col)
        data.append(globals()["trace%s" % x])

    fig = go.Figure(data=data)
    plot(fig, filename=filepath, auto_open=auto_open)


def plot_trip_map(df_out, map_filepath):

    import numpy as np
    import folium

    location_available = False
    if ("GPS_LAT" in df_out.columns) and ("GPS_LON" in df_out.columns):
        location_available = np.logical_and(
            df_out["GPS_LAT"].any(), df_out["GPS_LON"].any()
        )

    if location_available:
        fdata = df_out[["TIME", "GPS_LAT", "GPS_LON"]].dropna()
        coordinates = fdata[["GPS_LAT", "GPS_LON"]]

        m = folium.Map(location=sum(coordinates.values) / len(coordinates.values))

        sw = coordinates.min().values.tolist()
        ne = coordinates.max().values.tolist()
        m.fit_bounds([sw, ne])

        folium.PolyLine(coordinates.values, color="red", weight=5, opacity=1.0).add_to(
            m
        )

        popup = folium.Popup("<b>Start</b><br>", max_width=200)
        folium.Marker(list(coordinates.values[0]), popup=popup).add_to(m)

        popup = folium.Popup("<b>Finish</b><br>", max_width=200)
        folium.Marker(list(coordinates.values[-1]), popup=popup).add_to(m)

        m.save(map_filepath)
    else:
        print("GPS data not available. Map will not be created.\n\n")
    return True
