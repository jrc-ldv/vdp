import pytest
import numpy as np
from vdp.utils.signals import Signal


class TestSignal:
    @pytest.fixture
    def signal(self):
        s = Signal(name="Test")
        s.t = [1, 2, 3, 4, 5]
        s.v = ["a", "b", "c", "d", "e"]
        return s

    def test_return_last_categorical(self, signal):
        assert signal.return_last_categorical(3) == "c"
        assert signal.return_last_categorical(0) == ""

    def test_interpolate(self, signal):
        assert signal.interpolate(3) == "c"
        assert signal.interpolate(0) == ""

    def test_plot(self, signal):
        # Just test that plot doesn't raise an exception
        try:
            signal.plot()
        except Exception:
            pytest.fail("Plot method raised an exception")
