import pandas as pd
import glob
import os
from pathlib import Path
from config import *
from functions_EDP.plotting import create_simple_interactive_plot_from_df, plot_trip_map
from vdp.utils.fix_signs import (
    fix_battery_current,
    fix_consumers_currents,
    fix_motor_current,
)
from vdp.utils.datasyncing import align_and_merge_data

from vdp import vdp_dsp

overwrite_previous_results = False

### CALCULATIONS
data_mapping_filepath = os.path.join(
    os.getcwd(), "configuration_data_demo.xlsx"
)  # WRITE HERE THE PATH TO THE DATA-MAPPING .xlsx FILE
exp_data_expand_dir = os.path.join(
    os.getcwd(), "outputs"
)  # WRITE HERE THE OUTPUT FOLDER FOR YOUR RESULTS
data_mapping_ts = pd.read_excel(
    data_mapping_filepath, sheet_name="Timeseries", index_col=0
)
data_mapping_filepaths = pd.read_excel(data_mapping_filepath, sheet_name="Datafiles")
data_mapping_inputs = pd.read_excel(
    data_mapping_filepath, sheet_name="Inputs", index_col=0
)

add_data = {}
for veh in data_mapping_inputs.columns:
    add_data[veh] = data_mapping_inputs[veh].dropna().to_dict()

map_columns = {}
load_cols = {}

files = glob.glob(os.path.join(exp_data_expand_dir, "*"))

for veh in data_mapping_ts.columns:
    map_columns[veh] = dict(
        data_mapping_ts.loc[:, veh].dropna().reset_index().iloc[:, [1, 0]].values
    )
    load_cols[veh] = [
        col for col in data_mapping_ts.loc[:, veh].values if not pd.isna(col)
    ]

for idx, (veh, test_name, filepath, sheet_name) in data_mapping_filepaths.iterrows():

    filepath = Path(filepath)

    filepath = os.path.join(os.getcwd(), filepath)
    (filedir, filename) = (os.path.dirname(filepath), os.path.basename(filepath))

    dsp_sol_filepath = os.path.join(exp_data_expand_dir, "_".join([veh, test_name]))
    if (os.path.exists(dsp_sol_filepath)) & (overwrite_previous_results == False):
        continue

    exp_data_exp_filepath = os.path.join(
        dsp_sol_filepath, "_".join([veh, test_name]) + "_output.xlsx"
    )
    exp_data_exp_filepath_reduced = os.path.join(
        dsp_sol_filepath, "_".join([veh, test_name]) + "_output_reduced.xlsx"
    )

    iplot_filepath = os.path.join(
        dsp_sol_filepath, "_".join([veh, test_name]) + "_output.html"
    )
    map_filepath = os.path.join(
        dsp_sol_filepath, "_".join([veh, test_name]) + "_map.html"
    )

    print("Vehicle: ", veh)
    print("Test name: ", test_name)
    print("Filepath: ", filepath)
    print("Expanded filepath: ", exp_data_exp_filepath)
    print("\n")

    extension = os.path.splitext(filepath)[1]
    is_excel = False
    if extension == ".xlsx":
        is_excel = True
        df_test = (
            pd.read_excel(filepath, sheet_name=sheet_name)
            if not pd.isnull(sheet_name)
            else pd.read_excel(filepath)
        )
    else:
        df_test = pd.read_csv(filepath, engine="python")

    if is_excel & set(["time_ref", "ref"]).issubset(df_test.columns):
        sheet_names = pd.ExcelFile(filepath).sheet_names
        sheets_to_align = [
            sheet_name for sheet_name in sheet_names if "align" in sheet_name
        ]
        for sheet_to_align in sheets_to_align:
            df_align = pd.read_excel(filepath, sheet_name=sheet_to_align)
            if set(["time_ref", "ref"]).issubset(df_align.columns):
                df_test = align_and_merge_data(
                    df_test, "time_ref", "ref", df_align, "time_ref", "ref"
                )

    all_vdp_data = [
        key
        for key, value in zip(vdp_dsp.nodes.keys(), list(vdp_dsp.nodes.values()))
        if value["type"] == "data"
    ]
    user_mapped_cols = list(set(load_cols[veh]).intersection(list(df_test)))
    available_cols_with_standard_name = [
        col for col in df_test.columns if col in all_vdp_data
    ]
    load_cols_union = list(
        set(user_mapped_cols) | set(available_cols_with_standard_name)
    )
    df_test = df_test[load_cols_union].rename(columns=map_columns[veh])

    cols_not_mapped = [col for col in df_test.columns if col not in all_vdp_data]
    for col in cols_not_mapped:
        print("Mapped field not existing in model: %s" % col)

    if ("V_HVBAT" in list(df_test)) & ("I_HVBAT" in list(df_test)):
        V, I = fix_battery_current(df_test["V_HVBAT"], df_test["I_HVBAT"])
        df_test["V_HVBAT"] = V
        df_test["I_HVBAT"] = I

    if "I_DCDC_LV" in list(df_test):
        df_test["I_DCDC_LV"] = (
            df_test["I_DCDC_LV"].values
            if df_test["I_DCDC_LV"].quantile(0.5) < 0
            else -df_test["I_DCDC_LV"].values
        )

    if "I_MOTOR" in list(df_test):
        I = fix_motor_current(df_test["V_HVBAT"], df_test["I_MOTOR"])
        df_test["I_MOTOR"] = I

    ts_to_fix = [
        col
        for col in list(df_test)
        if col in ["I_DCDC_HV", "I_PTC_CABIN", "I_AC_COMP", "I_MAC"]
    ]
    dict_to_fix = df_test[ts_to_fix].to_dict(orient="series")

    fixed_dict = fix_consumers_currents(dict_to_fix)

    for key, values in fixed_dict.items():
        df_test[key] = values

    # dict_exp = df_test.to_dict(orient="series")
    dict_exp = {col: df_test[col].values for col in df_test.columns}
    dict_add = add_data[veh]
    dict_dsp = {**dict_initialise, **dict_exp, **dict_add}

    # shrinked_outputs = ["E_ice_neg_at_wheels",
    #     "E_ems_neg_at_wheels",
    #     "E_friction_brakes",
    #     "E_wheels_neg_cumulative"]
    #
    # shrinked_vdp = vdp_dsp.shrink_dsp(outputs=shrinked_outputs)
    # sol = shrinked_vdp.dispatch(inputs=dict_dsp)

    sol = vdp_dsp.dispatch(inputs=dict_dsp)
    sol.plot(directory=dsp_sol_filepath, view=True)

    df_sol = pd.DataFrame.from_dict(sol, orient="columns")
    df_out = pd.DataFrame()

    # for col in ordered_columns:
    #     if col in list(df_sol):
    #         df_out[col] = df_sol[col].values

    col_ordered_list = [col for col in ordered_columns if col in list(df_sol)]
    df_out = pd.concat([df_out, df_sol[col_ordered_list]], axis=1)

    col_missing_list = list(set(list(df_sol)) - set(list(df_out)))

    df_out = pd.concat([df_out, df_sol[col_missing_list]], axis=1)

    to_drop = list_GPS + cols_to_drop

    df_out.drop(columns=list(set(to_drop).intersection(list(df_out))), inplace=True)

    if not os.path.exists(dsp_sol_filepath):
        Path(dsp_sol_filepath).mkdir(parents=True, exist_ok=True)

    df_out.to_excel(exp_data_exp_filepath, index=False)

    cols_out_reduced = list(set(list_cols_reduced).intersection(list(df_out)))

    df_out_red = pd.DataFrame()
    for col in list_cols_reduced:
        if col in list(df_out):
            df_out_red[col] = df_out[col]

    # df_out_red.to_excel(exp_data_exp_filepath_reduced, index=False)

    df_out.drop(columns=list(set(to_drop_2).intersection(list(df_out))), inplace=True)

    if len(set(list_GPS).intersection(list(df_out))) > 0:
        print("Proxy for GPS accuracy: AVAILABLE.\n")
    else:
        print("Proxy for GPS accuracy: NOT AVAILABLE.\n")

    if True:  #'1hz.' in filepath:
        create_simple_interactive_plot_from_df(
            df_out_red, iplot_filepath, auto_open=True
        )

    print("OBD SPEED correction factor: ", sol["OBD_SPEED_CORRECTION_FACTOR"])

    plot_trip_map(df_out, map_filepath)

    print("\n\n\n")
