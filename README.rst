.. figure:: ./doc/_static/images/logo.png
    :align: center
    :alt: alternate text
    :figclass: align-center

.. _start-info:

:versions:      |gh-version| |rel-date| |python-ver|
:documentation: Under construction  |doc|
:sources:       https://code.europa.eu/jrc-ldv/vdp  |codestyle|
:keywords:      vehicle, data processing, real data.
:short name:    VDP
:live-demo:     CED calculation tool:  |binder|
:contact:     JRC-LDVS-CO2@ec.europa.eu
:Copyright and License:     © Copyright (c) 2021 European Union.

            Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission – subsequent versions of the EUPL (the "Licence");
            You may not use this work except in compliance with the Licence.
            You may obtain a copy of the Licence at: |proj-lic|

            Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS
            OF ANY KIND, either express or implied. See the Licence for the specific language governing permissions and limitations under the Licence.

A python-3.10+ package to process vehicle data.

.. role:: Warning
    
:Warning: `This tool is under development, all the calculations and results are to be considered preliminary.`

.. _end-info:

.. contents:: Table of Contents
    :backlinks: top

.. _start-intro:

Introduction
============

Overview
--------

.. _end-intro:

.. _start-installation:

Installation
============

Prerequisites
-------------
**Python-3.7+** is required and **Python-3.10** recommended.
It requires **numpy/scipy** and **pandas** libraries with native backends.

.. Tip::
    On *Windows*, it is preferable to use the `Anaconda <https://www.anaconda.com/products/individual>`__ distribution.
    To avoid possible incompatibilities with other projects

.. Tip::
    Comand to create a conda enviroment with a specific python distribution::
        
        conda create -n myenv python=x.x

Download
--------
Download the sources,

- either with *git*, by giving this command to the terminal::

    git clone https://code.europa.eu/jrc-ldv/vdp.git

Install
-------
From within the project directory, run one of these commands to install it:

- for standard python, installing with ``pip`` is enough (but might)::

        pip install -e <path_to_vdp>


.. _end-installation:

.. _start-sub:

.. |python-ver| image::  https://img.shields.io/badge/PyPi%20python-3.9%20%7C%203.10-informational
    :alt: Supported Python versions of latest release in PyPi

.. |gh-version| image::  https://img.shields.io/badge/GitHub%20release-dev-orange
    :target: https://code.europa.eu/jrc-ldv/vdp
    :alt: Latest version in GitHub

.. |rel-date| image:: https://img.shields.io/badge/rel--date-14--07--2022-orange
    :target: https://code.europa.eu/jrc-ldv/vdp
    :alt: release date

.. |br| image:: https://img.shields.io/badge/docs-working%20on%20that-red
    :alt: GitHub page documentation

.. |doc| image:: https://img.shields.io/badge/docs-passing-success
    :alt: GitHub page documentation

.. |proj-lic| image:: https://img.shields.io/badge/license-European%20Union%20Public%20Licence%201.2-lightgrey
    :target:  https://joinup.ec.europa.eu/software/page/eupl
    :alt: EUPL 1.2

.. |codestyle| image:: https://img.shields.io/badge/code%20style-black-black.svg
    :target: https://github.com/ambv/black
    :alt: Code Style

.. |pypi-ins| image:: https://img.shields.io/badge/pypi-v1.1.3-informational
    :target: https://pypi.org/project/wltp-jrshift/
    :alt: pip installation

.. |binder| image:: https://mybinder.org/badge_logo.svg
    :target: https://mybinder.org/v2/git/https%3A%2F%2Fcode.europa.eu%2Fjrc-ldv%2Fvdp/HEAD?labpath=Notebooks%2FGUI_binder_CED_interface.ipynb
    :alt: JupyterLab for CED Calculation Tool (stable)

.. |CO2| replace:: CO\ :sub:`2`
.. _end-sub:


