# -*- coding: utf-8 -*-
#
# Copyright 2013-2022 European Commission (JRC);
# Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
# – subsequent versions of the EUPL (the "Licence");
#
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
"""
Vehicle data processing
"""
import io
import os
import collections
import os.path as osp
import functools
from setuptools import setup, find_packages

name = "VDP"

module_name = "vdp"

mydir = osp.dirname(__file__)


def read_project_version():
    fglobals = {}
    with io.open(os.path.join(mydir, module_name, "_version.py")) as fd:
        exec(fd.read(), fglobals)  # To read __version__
    return fglobals["__version__"]


def read_file(fpath):
    with open(fpath) as fd:
        return fd.read()


# TODO: Update url before release
proj_ver = read_project_version()
url = f"https://github.com/tansial/{name}"
download_url = f"{url}/releases/tag/{proj_ver}"
project_urls = {
    "Documentation": url,
    "Sources": "url",
    "Bug Tracker": f"{url}/issues",
    "Live Demo": url,
}

extras = {
    "cli": ["click", "click-log"],
    "sync": ["pandas>=1.5.1"],
    "plot": [
        "flask",
        "regex",
        "graphviz",
        "Pygments",
        "lxml",
        "jinja2",
        "docutils",
        "plotly",
    ],
    "io": ["pandas>=1.5.1", "regex", "xlrd",],
}

extras["all"] = list(functools.reduce(set.union, extras.values(), set()))

extras["dev"] = extras["all"] + [
    "wheel",
    "sphinx",
    "gitchangelog",
    "mako",
    "sphinx_rtd_theme",
    "setuptools>=36.0.1",
    "sphinxcontrib-restbuilder",
    "nose",
    "coveralls",
    "ddt",
    "sphinx-click",
    "sphinx-autodocgen",
]

setup(
    name=name,
    version=proj_ver,
    packages=find_packages(exclude=["test", "test.*", "doc", "doc.*", "appveyor"]),
    license="EUPL 1.1+",
    author="VDP-Team",
    author_email="jrc-ldvs-co2@ec.europa.eu",
    description="Vehicle Experimental Data Processing",
    long_description=read_file("README.rst"),
    keywords="""Vehicle data processing
    """.split(),
    classifiers=[
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "Programming Language :: Python :: Implementation :: CPython",
        "Development Status :: 1 - Beta",
        "Natural Language :: English",
        "Intended Audience :: Developers",
        "Intended Audience :: Science/Research",
        "Intended Audience :: Manufacturing",
        "License :: OSI Approved :: European Union Public Licence 1.2 " "(EUPL 1.2)",
        "Natural Language :: English",
        "Operating System :: MacOS :: MacOS X",
        "Operating System :: Microsoft :: Windows",
        "Operating System :: POSIX",
        "Operating System :: Unix",
        "Operating System :: OS Independent",
        "Topic :: Scientific/Engineering",
        "Topic :: Scientific/Engineering :: Information Analysis",
        "Topic :: Scientific/Engineering :: Data Processing",
    ],
    python_requires=">=3.7",
    install_requires=[
        "numpy>=1.23.0",
        "pandas>=1.5.0",
        "scipy>=1.13.1",
        "schedula[all]==1.2.19",
        "plotly>=5.10.0",
        "graphviz>=0.20.1",
        "folium>=0.12.1",
        "openpyxl>=3.0.10",
        "regex>=2022.9.13",
        "docutils<0.18",
        "srtm-py>=0.3.7",
        "gpxpy>=1.5.0",
        "co2mpas",
        "cantools>=39.4.5",
        "pydantic>=2.7.1",
        "syncing>=1.0.9",
        "statsmodels>=0.14.2",
    ],
    # entry_points={"console_scripts": [f"{module_name} = {module_name}.cli:cli"]},
    # extras_require=extras,
    # tests_require=["nose>=1.0", "ddt"],
    # test_suite="nose.collector",
    # package_data={"vdp": ["demos/*.xlsx", "core/load/speed_phases/*.ftr"]},
    zip_safe=True,
    options={"bdist_wheel": {"universal": True}},
    platforms=["any"],
)
